package com.extremefin.finstinct.opennlp.summarization.preprocess;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.util.Span;

public class OrganizationFinder {
	 @Value("classpath:config/en-ner-organization.bin")
		private Resource sentModelFile;
	public static void main(String[] args) {
		// find person name
		try {
			System.out.println("-------Finding entities belonging to category : person name------");
			new OrganizationFinder().findName();
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// find place
		try {
			System.out.println("-------Finding entities belonging to category : place name------");
			new OrganizationFinder().findLocation();
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// find place
				try {
					System.out.println("-------Finding entities belonging to category : organization name------");
					new OrganizationFinder().findOrganization();
					System.out.println();
				} catch (IOException e) {
					e.printStackTrace();
				}
	}
 
	/**
	 * method to find locations in the sentence
	 * @throws IOException
	 */
	public void findName() throws IOException {
		InputStream is = new FileInputStream("en-ner-person.bin");
 
		// load the model from file
		TokenNameFinderModel model = new TokenNameFinderModel(is);
		is.close();
 
		// feed the model to name finder class
		NameFinderME nameFinder = new NameFinderME(model);
 
		// input string array
		String[] sentence = new String[]{
				"John",
				"Smith",
				"is",
				"standing",
				"next",
				"to",
				"bus",
				"stop",
				"and",
				"waiting",
				"for",
				"Mike",
				"."
		};
 
		Span nameSpans[] = nameFinder.find(sentence);
 
		// nameSpans contain all the possible entities detected
		for(Span s: nameSpans){
			System.out.print(s.toString());
			System.out.print("  :  ");
			// s.getStart() : contains the start index of possible name in the input string array
			// s.getEnd() : contains the end index of the possible name in the input string array
			for(int index=s.getStart();index<s.getEnd();index++){
				System.out.print(sentence[index]+" ");
			}
			System.out.println();
		}
	}
	
	/**
	 * method to find locations in the sentence
	 * @throws IOException
	 */
	public void findLocation() throws IOException {
		InputStream is = new FileInputStream("en-ner-location.bin");
 
		// load the model from file
		TokenNameFinderModel model = new TokenNameFinderModel(is);
		is.close();
 
		// feed the model to name finder class
		NameFinderME nameFinder = new NameFinderME(model);
 
		// input string array
		String[] sentence = new String[]{
				"John",
				"Smith",
				"is",
				"from",
				"Atlanta",
				"."
		};
 
		Span nameSpans[] = nameFinder.find(sentence);
 
		// nameSpans contain all the possible entities detected
		for(Span s: nameSpans){
			System.out.print(s.toString());
			System.out.print("  :  ");
			// s.getStart() : contains the start index of possible name in the input string array
			// s.getEnd() : contains the end index of the possible name in the input string array
			for(int index=s.getStart();index<s.getEnd();index++){
				System.out.print(sentence[index]+" ");
			}
			System.out.println();
		}
	}
	
	/**
	 * method to find locations in the sentence
	 * @throws IOException
	 */
	public void findOrganization() throws IOException {
		//Resource resource = Resource.class.getClassLoader().
		InputStream is = new FileInputStream("C:/Users/Shiva/workspace/Development/finstinct/finstinct-app/finstinct-core/src/main/resources/config/en-ner-organization.bin");
//		InputStream is = null;
//		is = sentModelFile.getInputStream();
		// load the model from file
		TokenNameFinderModel model = new TokenNameFinderModel(is);
		is.close();
 
		// feed the model to name finder class
		NameFinderME nameFinder = new NameFinderME(model);
 
		String sent="The Indian National Lok Dal (INLD) on Monday accused the ruling Congress government in Haryana of having indulged in a massive land scam worth hundreds of crores of rupees Party leaders also revealedwhat they saidwere links between various firms and Onkareshwar Properties Private Limited ? the company that sold land to Congress president Sonia Gandhi?s son-in-law Robert Vadra The Opposition party singled out senior Congress leader Venod Sharma and alleged that his close associates became directors of companies that kept getting licensesfor commercial and residential coloniesfrom the Haryana government around the time the Vadra-DLF land deals were taking place In a press conference hereINLD leadersAbhay Chautala and the party?s state president Ashok Arorafurther alleged that because of this rampant licensingland prices ? in areas where developers? licenses were issued to ?certain favourites? ? escalated manifold They claimed that three companies ? Onkareshwar Properties LimitedMarkbuild Tech and Lakshyabuild Tech ? had financial transactions with companies owned by Venod Sharma?s son Kartikey Sharma The INLD leaders added that according to the website of Ministry of Corporate Affairsthe three companies also shared the same registered office between 2008 and 2011 ? Flat No 714Hemkunth Chambers89Nehru PlaceNew Delhi It was subsequently changed to Flat No 723Hemkunth Chambers89Nehru Place and after 2012the addresses of all the three companies were shifted to Satyanand Yajee?s residence in Neb SaraiNew Delhi Satyanand Yajee was the director of Onkareshwar Properties in 2008when he sold 35 acres of land in Shikhopur villageGurgaonto M/s Sky Light Hospitality Private Limited ? a company owned by Robert Vadra It is this land that Vadra sold to M/s DLF Retail Developers Private Limited (now known as DLF Universal Limited) ?Venod Sharma?s son Kartikeya is the director of Information TV Private Limited In 2009 and 2010Onkareshwar Properties? balance sheets show that the company invested Rs 3 crore in Information TV Private Limited In 2010Onkareshwar Properties invested Rs 25 crore in the same company Kartikeya Sharma?s Information TV Private Limited also has financial relationship with the Vatika group? Abhay Chautala alleged ?The Vatika group?s directors have also been associated with Onkareshwar Properties In factafter the land was sold to Vadra?s companythe Haryana government granted three land licenses to the Vatika group After the licenses were granted to Vatika groupthe prices of the land sold by Vadra to DLF touched approximately Rs 500 crore Vatika group?s assets after February 192008rose to Rs 209 crore within a year In the next three yearsthe company?s documents show that it recorded a Rs 85-crore profit and Rs 71 crore as cash/ bank balance The company also has Rs 136 crore in reserves and it has handed out Rs 5638 crore as loans? the INLD leaders alleged Further targetting Venod Sharmathe Opposition leaders said that the firmMarkbuild Techhas Sharma?s close associates as its directors ?The company?s directors include Dhirender DadwalHarvinder Chopra and Tarun Bhanot (one of Venod Sharma?s relatives) This company was also granted five licenses for land measuring 2849 acres?Arora said For all the latest India News download Indian Express App";
		sent=sent.replaceAll("\\?", "");		
		// input string array
		String[] sentence = this.getWords(sent);
 
		Span nameSpans[] = nameFinder.find(sentence);
 
		// nameSpans contain all the possible entities detected
		for(Span s: nameSpans){
			System.out.print(s.toString());
			System.out.print("  :  ");
			// s.getStart() : contains the start index of possible name in the input string array
			// s.getEnd() : contains the end index of the possible name in the input string array
			for(int index=s.getStart();index<s.getEnd();index++){
				System.out.print(sentence[index]+" ");
			}
			System.out.println();
		}
	}
	
	public String[] getWords(String sent)
	{
		return sent.split(" ");
	}
}
