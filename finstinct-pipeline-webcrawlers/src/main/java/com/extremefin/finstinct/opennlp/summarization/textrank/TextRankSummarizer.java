/*
 	* Licensed to the Apache Software Foundation (ASF) under one or more
 	* contributor license agreements. See the NOTICE file distributed with
 	* this work for additional information regarding copyright ownership.
 	* The ASF licenses this file to You under the Apache License, Version 2.0
 	* (the "License"); you may not use this file except in compliance with
 	* the License. You may obtain a copy of the License at
 	*
 	* http://www.apache.org/licenses/LICENSE-2.0
 	*
 	* Unless required by applicable law or agreed to in writing, software
 	* distributed under the License is distributed on an "AS IS" BASIS,
 	* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 	* See the License for the specific language governing permissions and
 	* limitations under the License.
*/

package com.extremefin.finstinct.opennlp.summarization.textrank;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.extremefin.finstinct.opennlp.summarization.*;
import com.extremefin.finstinct.opennlp.summarization.preprocess.DefaultDocProcessor;
import com.extremefin.finstinct.opennlp.summarization.preprocess.IDFWordWeight;
import com.extremefin.finstinct.opennlp.summarization.preprocess.WordWeight;

/*
 * A wrapper around the text rank algorithm.  This class
 * a) Sets up the data for the TextRank class
 * b) Takes the ranked sentences and does some basic rearranging (e.g. ordering) to provide a more reasonable summary.
 */
public class TextRankSummarizer implements Summarizer
{
	//An optional file to store idf of words. If idf is not available it uses a default equal weight for all words.
    private String idfFile = "resources/idf.csv";
    public TextRankSummarizer() throws Exception
    {
    }

    class SortBySentId implements Comparator<Score>{

    	public int compare(Score a, Score b){
    		if(a.getSentId() > b.getSentId()) return 1;
    		else return -1;
    	}
    }

    /*Sets up data and calls the TextRank algorithm..*/
    public List<Score> rankSentences(String doc, List<Sentence> sentences,
    							     DocProcessor dp, int maxWords )
    {
        try {
    	    //Rank sentences
            TextRank summ = new TextRank(dp);
            List<String> sentenceStrL = new ArrayList<String>();
            List<String> processedSent = new ArrayList<String>();
            Hashtable<String, List<Integer>> iidx = new Hashtable<String, List<Integer>>();
       //     dp.getSentences(sentences, sentenceStrL, iidx, processedSent);

            for(Sentence s : sentences){
            	sentenceStrL.add(s.getStringVal());
            	String stemmedSent = s.stem();
            	processedSent.add(stemmedSent);

            	String[] wrds = stemmedSent.split(" ");
            	for(String w: wrds)
            	{
            		if(iidx.get(w)!=null)
            			iidx.get(w).add(s.getSentId());
            		else{
            			List<Integer> l = new ArrayList<Integer>();
            			l.add(s.getSentId());
            			iidx.put(w, l);
            		}
            	}
            }

            WordWeight wordWt = new IDFWordWeight(idfFile);////new

    	    List<Score> finalScores = summ.getRankedSentences(doc, sentenceStrL, iidx, processedSent);
    	    List<String> sentenceStrList = summ.getSentences();

    	   // SentenceClusterer clust = new SentenceClusterer();
    	   //  clust.runClusterer(doc, summ.processedSent);

    		Hashtable<Integer,List<Integer>> links= summ.getLinks();

			for(int i=0;i<sentences.size();i++)
			{
				Sentence st = sentences.get(i);

				//Add links..
				List<Integer> currLnks = links.get(i);
				if(currLnks==null) continue;
				for(int j=0;j<currLnks.size();j++)
				{
					if(j<i) st.addLink(sentences.get(j));
				}
			}

			for(int i=0;i<finalScores.size();i++)
			{
				Score s = finalScores.get(i);
				Sentence st = sentences.get(s.getSentId());
				st.setPageRankScore(s);
			}

			List<Score> reRank = finalScores;//reRank(sentences, finalScores, iidx, wordWt, maxWords);

			return reRank;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }

    //Returns the summary as a string.
	@Override
	public String summarize(String article, DocProcessor dp, int maxWords,String description,String title,String alt) {
        List<Sentence> sentences = dp.getSentencesFromStr(article);
    //   String freshArticle=this.nonRepitiveFreshArticle(sentences);
      // List<Sentence> sentencesFresh = dp.getSentencesFromStr(freshArticle);
   //     List<Score> scores = this.rankSentences(article, sentences, dp, maxWords);
       List<Score> scores = this.rankSentences(article, sentences, dp, maxWords);
       // return scores2String(sentences, scores, maxWords);
       return scores2StringWithDescription(sentences, scores, maxWords,description,title,alt);
	}

	private String nonRepitiveFreshArticle(List<Sentence> sentences) {
		// TODO Auto-generated method stub
		 StringBuffer b = new StringBuffer();
	       // for(int i=0;i< min(maxWords, scores.size()-1);i++)
	        int i=0;
	        while(i< sentences.size())
	        {
	        	String sent = sentences.get(i).getStringVal();	
	        	sent = sent.replaceAll("\n","");
	        	sent = sent.trim();
	            // do not add duplicate sentences
	            if(b.indexOf(sent) >= 0) {
	            	int start= b.indexOf(sent);
	            	int end = start + sent.length()-1;
	            	b.replace(start, end, "");
	            }else{
	        	b.append(sent);
	            }
	        	i++;
	        }
	        return b.toString();
	}

	@Override
	public String summarizeone(String article, DocProcessor dp, int maxWords,List<String> map,String p) {
		//Iterator mapIterator = 
		 List<Sentence> sentencesone = dp.getSentencesFromStr(p);
		 List<Sentence> sentences = dp.getSentencesFromStr(article);
		 Collections.reverse(map);
		 for (String key : map)
		 {
			 String[] keyCheck = key.split(" ");
			 if(keyCheck.length > 4){
				 continue;
			 }
		     //String keyValue12 = map.get(key);
		     for(int i = 0; i < sentencesone .size(); i++)
		      {
		    	 try{
		    	String a= sentencesone.get(i).getStringVal();
		    	String c = a;
//		    	if(a.contains(key)){
		    	if(isContain(a,key)){
	    			  a=a.toLowerCase().replaceAll(key.toLowerCase(),"");
	    			//  a = a.replaceAll("[^\\w\\s\\?]","");
	    			  a = a.replaceAll("\\s+", " ").trim();
		    		  for(int j = 0; j < sentences .size(); j++){
		    			  try{
		    			  String b= sentences.get(j).getStringVal().toLowerCase().replaceAll(key.toLowerCase(),"");
		    			//  b=b.replaceAll("[^\\w\\s\\?]","");
		    			  b=b.replaceAll("\\s+", " ").trim();
//		    			  Pattern pattern = Pattern.compile(a);
//		    			  Matcher m = pattern.matcher(b);
//							if (m.matches()){
//								 String newString = b.replace(a, c);
//							sentences.get(j).setStringVal(newString);
//							}
		    		    		if(b.toLowerCase().contains(a.toLowerCase()))
		    		    		{
//		    		    				c = c.replaceAll(key,
//		    									"<a href=\"" + keyValue + "\"/>" + key + "</a>");
		    		    			 String newString = b.replace(a, c);
		    		    			sentences.get(j).setStringVal(newString);
		    		    		}else if(a.toLowerCase().contains(b.toLowerCase())){
//		    		    			 String newString = b.replace(a, c);
//			    		    			sentences.get(j).setStringVal(newString);
			    		    			String toInser= c;
			    		    			toInser=toInser.replaceAll("[^\\w\\s\\?]","");
			    		    			toInser=toInser.replaceAll("\\s+", " ");
			    		    			String[] items = b.split(" ");
			    		    			boolean matchFound=false;
			    		    			for(int x=0;x<=items.length;x++){
			    		    				String newStringToCheck="";
			    		    				for(int y=0;y<items.length;y++){
			    		    					if(y==0){
				    		    				if(x==y){
				    		    					newStringToCheck=key+" "+items[y];
				    		    				}else{
				    		    					newStringToCheck = items[y];
				    		    				}
			    		    					}else{
			    		    						if(x==y){
					    		    					newStringToCheck=newStringToCheck+" "+key+" "+items[y];
					    		    				}else {
					    		    					if(x == items.length){
						    		    					if(x == y+1){
							    		    					newStringToCheck=newStringToCheck+" "+items[y]+" "+key;
						    		    					}else{
							    		    					newStringToCheck=newStringToCheck+" "+items[y];
						    		    					}
						    		    				}else{
					    		    					newStringToCheck=newStringToCheck+" "+items[y];
						    		    				}
					    		    				}
			    		    					}
				    		    			}	
			    		    				newStringToCheck=newStringToCheck.replaceAll("[^\\w\\s\\?]","");
			    		    				newStringToCheck=newStringToCheck.replaceAll("\\s+", " ");
			    		    				String a1=newStringToCheck.replaceAll(" ", "");
			    		    				String a2=c.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").replaceAll(" ", "");
			    		    				if(a2.toLowerCase().contains(a1.toLowerCase())){
				    		    				String[] newStringArray = sentences.get(j).getStringVal().split(" ");
				    		    				String toInsertInSent="";
				    		    				for(int z=0;z<newStringArray.length;z++){
				    		    					if(x == newStringArray.length){
				    		    						toInsertInSent=sentences.get(j).getStringVal()+" "+key;
				    		    					}else{
				    		    						if(z==0){
				    		    							if(z==x){
					    		    							toInsertInSent = key+" "+newStringArray[z];
					    		    						}else{
					    		    							toInsertInSent = newStringArray[z];
					    		    						}
				    		    						}else{
				    		    							if(z==x){
					    		    							toInsertInSent = toInsertInSent+" "+key+" "+newStringArray[z];
					    		    						}else{
					    		    							toInsertInSent = toInsertInSent+" "+newStringArray[z];
					    		    						}
				    		    						}
				    		    					}
				    		    				}
				    		    				matchFound=true;
				    		    				//insert new sent here
				    		    				sentences.get(j).setStringVal(toInsertInSent);
				    		    				break;
			    		    				}
			    		    			}
			    		    			if(!matchFound){
			    		    				String[] keyLength = key.split(" ");
			    		    				int start= a.indexOf(b);
			    		                	int end = start + b.length()-1;
			    		                	String[] stringArrray=c.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").toLowerCase().split(" ");
			    		    				int counter=0;
			    		    				for (String string : stringArrray) {
												if(string.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").toLowerCase().contains(key.toLowerCase())){
													counter++;
												}
											}
			    		                	//b.replace(start, end, "");
			    		                	if(start > key.length()){
			    		                		start = start-(key.length()*counter)-keyLength.length;
			    		                		end = end + (key.length()*counter)+keyLength.length;
			    		                	}else{
			    		                		end = end + (key.length()*counter)+keyLength.length;
			    		                	}
			    		                	
			    		                	String newFreshRequired = c.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").substring(start,end);
			    		    				
			    		                	String[] arrayOfB = b.split(" ");
			    		    				String[] stringArrray2=newFreshRequired.toLowerCase().split(" ");
			    		    				String word="";
			    		    				Integer indexOfWord=0;
			    		    				Boolean wordFord=false;
			    		    				for (String string : stringArrray2) {
												if(string.equalsIgnoreCase(key)){
													//word = string;
													wordFord=true;
													//break;
												}
												if(string.equalsIgnoreCase(arrayOfB[0])){
												//	word = string;
													wordFord=true;
													//break;
												}
												if(wordFord && indexOfWord < arrayOfB.length){
													word=word+" "+string;
													indexOfWord++;
												}
											}
			    		    				
			    		    				String newSent = word.substring(0, 1).toUpperCase() + word.substring(1);
			    		    				sentences.get(j).setStringVal(newSent+" . ");
			    		    			}
		    		    		}
		    			  }catch(Exception exception){
		    				  continue;
		    			  }
		    			  
		    			  
		    		  }
		    	}
		    	 }catch(Exception exception){
   				  continue;
   			  }
		       }

		 }
        List<Score> scores = this.rankSentences(article, sentences, dp, maxWords);
        return scores2String(sentences, scores, maxWords);
	}
	/* Use the page rank scores to determine the summary.*/
    public String scores2String(List<Sentence> sentences, List<Score> scores, int maxWords)
    {
        StringBuffer b = new StringBuffer();
       // for(int i=0;i< min(maxWords, scores.size()-1);i++)
        int i=0;
        while(i< scores.size())
        {
        	String sent = sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("(?i)\\b([a-z]+)\\b(?:\\s+\\1\\b)+", "$1");	
        	sent = sent.replaceAll("\n","");
        	sent = sent.trim();
            // do not add duplicate sentences
            if(b.indexOf(sent) >= 0) {
            	int start= b.indexOf(sent);
            	int end = start + sent.length()-1;
            	b.replace(start, end, "");
            }else{
        	b.append(sent);
            }
        	i++;
        }
        return b.toString();
    }

    /* Use the page rank scores to determine the summary.*/
    public String scores2StringWithDescription(List<Sentence> sentences, List<Score> scores, int maxWords,String description,String title,String alt)
    {
    	List<String> newSentences = new ArrayList<>();
    	List<String> currentSentences = new ArrayList<>();
    	Integer descFoundCounter=0;
    	Integer titleAltFoundCounter=0;
        StringBuffer b = new StringBuffer();
       // for(int i=0;i< min(maxWords, scores.size()-1);i++)
        int i=0;
        while(i< scores.size())
        {
        	String sent = sentences.get(scores.get(i).getSentId()).getStringVal();	
        	sent = sent.replaceAll("\n","");
        	sent=sent.replaceAll("[^\\w\\s\\?]","");
        	sent=sent.replaceAll("\\s+", " ");
        	sent = sent.trim();
        	if(i<3){

        		Boolean d = false;
        		Boolean a = false;
        		String freshdesc = description.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim();
        		if(sent.toLowerCase().contains(freshdesc.toLowerCase())){
        			d = true;
        			descFoundCounter++;
        			if(descFoundCounter == 1){
        				String[] arrayStrChk = sentences.get(scores.get(i).getSentId()).getStringVal().split(" ");
        				if(!arrayStrChk[arrayStrChk.length-1].contains(".")){
        					String sentNew = sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("\n","").trim()+".";
        					currentSentences.add(sentNew);	
        				}else{
        				currentSentences.add(sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("\n","").trim());
        				}
        			}
        		}
//        		if(!alt.isEmpty()){
//        		String freshalt = alt.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim();
//        		if(sent.toLowerCase().contains(freshalt.toLowerCase())){
//        			titleAltFoundCounter++;
//        			if(titleAltFoundCounter == 1 && d == false){
//        				if(!freshalt.toLowerCase().contains(freshdesc.toLowerCase()) && !freshalt.equalsIgnoreCase(freshdesc)){
//        					a = true;
//        				currentSentences.add(sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("\n","").trim());
//        				}
//        			}
//        		}
//        		}
//        		if(a== false && d == false){
        		if(d == false){
        			String[] arrayStrChk = sentences.get(scores.get(i).getSentId()).getStringVal().split(" ");
    				if(!arrayStrChk[arrayStrChk.length-1].contains(".")){
    					String sentNew = sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("\n","").trim()+".";
    					currentSentences.add(sentNew);	
    				}else{
    			
        			currentSentences.add(sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("\n","").trim());
    				}
        		}
        	}else{
        		Boolean f = false;
        		String freshdesc = description.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim();
        		if(sent.toLowerCase().contains(freshdesc.toLowerCase())){
        			f=true;
        		}
//        		if(!alt.isEmpty()){
//        		String freshalt = alt.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim();
//        		if(sent.toLowerCase().contains(freshalt.toLowerCase())){
//        			f=true;
//        		}
//        		}
        		if(!f){
        			String[] arrayStrChk = sentences.get(scores.get(i).getSentId()).getStringVal().split(" ");
    				if(!arrayStrChk[arrayStrChk.length-1].contains(".")){
    					String sentNew = sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("\n","").trim()+".";
    					currentSentences.add(sentNew);	
    				}else{
    			
        			currentSentences.add(sentences.get(scores.get(i).getSentId()).getStringVal().replaceAll("\n","").trim());
    				}
        		}
        	}
            i++;
        }
        
        if(descFoundCounter == 0){
        	String[] arrayStrChk = description.split(" ");
			if(!arrayStrChk[arrayStrChk.length-1].contains(".")){
				String sentNew = description.replaceAll("\n","").trim()+".";
				newSentences.add(sentNew);
			}else{
		
				newSentences.add(description.replaceAll("\n","").trim());
			}
        //	newSentences.add(description.replaceAll("\n","").trim());
        }
//    	if(!alt.isEmpty()){
//        if(titleAltFoundCounter == 0){
//        	String freshdesc = description.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim();
//        	String freshalt = alt.replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim();
//        	if(!freshalt.contains(freshdesc) && !freshalt.equalsIgnoreCase(freshdesc)){
//        		newSentences.add(alt.replaceAll("\n","").trim());
//        	}
//        }
//    	}
    	newSentences.addAll(currentSentences);
    	List<Integer> sentOrderNumber = new ArrayList<>();
    	for(int k=0;k<newSentences.size();k++){
    		for(int l=0;l<newSentences.size();l++){
        		if(k != l){
        			if(newSentences.get(k).replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim().contains(newSentences.get(l).replaceAll("[^\\w\\s\\?]","").replaceAll("\\s+", " ").trim())){
        				sentOrderNumber.add(l);	
        			}
        		}
        	}
    	}
    	for (Integer integer : sentOrderNumber) {
			newSentences.remove(integer);
		}
    	Integer j=0;
    	while(j < newSentences.size()){
    		// do not add duplicate sentences
    		String[] arrayStrChk = newSentences.get(j).split(" ");
			if(!arrayStrChk[arrayStrChk.length-1].contains(".")){
				String sentNew = newSentences.get(j)+".";
	        	b.append(sentNew);
			}else{
	        	b.append(newSentences.get(j));
			}
        	j++;
    	}
        return b.toString();
    }
    
    @Override
    public String summarizeTopFourSentsInArticleOrder(String article, DocProcessor dp){
    	List<Sentence> sentences = dp.getSentencesFromStr(article);
        List<Score> scores = this.rankSentences(article, sentences, dp, -1);

        return scores2String(sentences, scores, true, 4);
    }

    /**
     * This method maintains the order of the article
     */
    public String scores2String(List<Sentence> sentences, List<Score> scores, boolean ascOrder, int limitSents){
    	StringBuffer b = new StringBuffer();
    	PriorityQueue<Score> queue = new PriorityQueue<Score>(limitSents, new SortBySentId());

    	int i = 0;
    	while(i < limitSents && i < scores.size()){
        // not including the first sentence which is the reference title
    		if(scores.get(i).getSentId() == 0) {
    			i++;
    			limitSents++; // Please note that if you need to use limitSents anywhere else, this loop should use another variable
    			continue;
    		}
    		queue.add(scores.get(i));
    		i++;
    	}

    	while(queue.peek() != null){
    		String sent = sentences.get(queue.poll().getSentId()).getStringVal();
    		sent = sent.replaceAll("\n","");
        // do not add duplicate sentences
        if(b.indexOf(sent.trim()) >= 0) continue;
      	b.append(sent);
    	}

    	return b.toString();
    }

	@Override
	public List<String> sentencesFromstring(DocProcessor dp,String article) {
		// TODO Auto-generated method stub
		List<String> newSentences = new ArrayList<String>();
		 List<Sentence> sentences = dp.getSentencesFromStr(article);
		 for (Sentence sentence : sentences) {
			newSentences.add(sentence.getStringVal());
		}
		return newSentences;
	}
	
	
	  private static boolean isContain(String source, String subItem){
	         String pattern = subItem;
	         Pattern p=Pattern.compile(pattern);
	         Matcher m=p.matcher(source);
	         return m.find();
	    }

	@Override
	public String summarizeForUnlisted(String article, DocProcessor dp, int maxWords, String description, String title,
			String alt) {
		   String LIVEMINT_TEXT_REMOVE = "More From Livemint";
		   String IIFL_TEXT_REMOVE = "A+ a- 0";
		   String IIFL_TEXT_REMOVE_NEW = "A+ a- 0";
		 String IIFL_TEXT_REMOVE_NEW_SPACE = " A+ a- 0 ";
		 String IIFL_TEXT_REMOVE_NEW_SPACE_DOT = " . A+ a- 0 ";
		 String ET_IGNORE_NEWS = "Get instant notifications from Economic Times";
		 String INDIA_INFOLINE_STRING = "indiainfoline";
		 String IIFL_SEBI_REGN_REMOVE = "SEBI Regn.";
		 String PHOTO_CREDIT_PATTERN = "(.*)(Photo.*?:.*?:)(.*)";
		 String FILE_PHOTO_CREDIT_PATTERN = "(.*)(File photo.*?:.*?:)(.*)";
		 String INDIA_INFOLINE_TIME_PATTERN = "(.*)(India Infoline News Service.*?IST)(.*)";
		 String INDIA_INFOLINE_DEMAT_STRING = "Open FREE Demat Account";
		 String INDIA_INFOLINE_FIRST_NAME_STRING = "First Name";
		 String BUSINESS_STANDARD_STRING = "business-standard";
		 String ET_IGNORE_NEWS_DEV = "Get instant notifications from . Economic";
		 String text = new String("UTF-8");
			text = "There is no text.";
		  List<Sentence> sentences = dp.getSentencesFromStr(article);
		    //   String freshArticle=this.nonRepitiveFreshArticle(sentences);
		      // List<Sentence> sentencesFresh = dp.getSentencesFromStr(freshArticle);
		   //     List<Score> scores = this.rankSentences(article, sentences, dp, maxWords);
		       List<Score> scores = this.rankSentences(article, sentences, dp, maxWords);
		       text = scores2String(sentences, scores, sentences.size());
		       text = text.trim();
				text = text.replace(System.getProperty("line.separator"), "");
				text = text.toString().replaceAll("\\n", " ");
				text = text.replaceAll("\\r?\\n", "");
				text = text.replaceAll("\n", " --linebreak-- ");
				text = text.replaceAll("\\.+", ".");
				// text = text.replaceAll("[^a-zA-Z0-9
				// .]|(?<!\\d)[.]|[.](?!\\d)", ". ");
				if (text.contains(INDIA_INFOLINE_FIRST_NAME_STRING)) {
					text = text.replaceAll("First Name", "");
				}
				text = text.replaceAll("\\.(\\s?[A-Za-z])", " . $1");
				text = text.replaceAll("\\s+", " ");
				
				text = text.replaceAll(IIFL_TEXT_REMOVE, "");
				text = text.replaceAll(IIFL_TEXT_REMOVE_NEW, "");
				text = text.replaceAll(IIFL_TEXT_REMOVE_NEW_SPACE, "");
				text = text.replaceAll(IIFL_TEXT_REMOVE_NEW_SPACE_DOT, "");
				text = text.replaceAll("|E-Paper Mumbai:", "");
				// replacement of currency
				text = text.replaceAll("₹", "Rs.");
				text = text.replaceAll("₹", "&#8377;");
				text = text.replaceAll("₹", "&#x20B9;");
				// text = text.replaceAll("₹", "<i class='fa
				// fa-inr'></i>");
				// text = text.replaceAll("₹", "\u20B9");
				// if SEBI Regn. comes in the text, ignore the news
//				if (text.contains(IIFL_SEBI_REGN_REMOVE))
//					continue;

				// remove "More from Livemint"
				text = text.replaceAll(LIVEMINT_TEXT_REMOVE, "");

				// if ET shows pop-up data, ignore the news
//				if (text.contains(ET_IGNORE_NEWS))
//					continue;
				// if ET shows pop-up data, ignore the news on dev server
//				if (text.contains(ET_IGNORE_NEWS_DEV))
//					continue;
				Pattern p11 = Pattern.compile("Article saved successfully to my page");
				Matcher m11 = p11.matcher(text);
				if (m11.find())
					text = m11.replaceAll("");

				Pattern p12 = Pattern.compile("You must be Logged in to save article");
				Matcher m12 = p12.matcher(text);
				if (m12.find())
					text = m12.replaceAll("");
				// text = text.replaceAll(m12.group(2), "");
				text = text.replaceAll(IIFL_TEXT_REMOVE, "");
				text = text.replaceAll(IIFL_TEXT_REMOVE, "");
				text = text.replaceAll(IIFL_TEXT_REMOVE_NEW, "");
				text = text.replaceAll(IIFL_TEXT_REMOVE_NEW_SPACE, "");
				text = text.replaceAll(IIFL_TEXT_REMOVE_NEW_SPACE_DOT, "");
				text = text.replace("0\n", "").replace("A+", "").replace("a-", "");
				text = text.replace("     0", "");
				text = text.replace("   0", "");
				text = text.replace("  0", "");
				// remove "More from Livemint"
				text = text.replaceAll(LIVEMINT_TEXT_REMOVE, "");

				// remove photo credit
				Pattern p = Pattern.compile(PHOTO_CREDIT_PATTERN, Pattern.DOTALL);
				Matcher m = p.matcher(text);
				if (m.matches())
					text = text.replace(m.group(2), "");

				p = Pattern.compile(FILE_PHOTO_CREDIT_PATTERN, Pattern.DOTALL);
				m = p.matcher(text);
				if (m.matches())
					text = text.replace(m.group(2), "");

				// remove IIFL Time and Place pattten e.g. India
				// Infoline News Service | Mumbai | September 10,
				// 2017
				// 12:45 IST
				p = Pattern.compile(INDIA_INFOLINE_TIME_PATTERN, Pattern.DOTALL);
				m = p.matcher(text);
				if (m.matches())
					text = text.replace(m.group(2), "");

				// remove everthing after this string - Open FREE
				// Demat
				// Account
				int indexOfSearchString = text.lastIndexOf(INDIA_INFOLINE_DEMAT_STRING);
				if (indexOfSearchString > 0)
					text = text.replace(text.substring(indexOfSearchString, text.length()), "");

				text = text.replaceAll("\\s+", " ");
//				String summaryWithoutDuplicates = removeDuplicate(summary);
//				summaryWithoutDuplicates = summaryWithoutDuplicates.replaceAll("\\.+", ".")
//						.replaceAll("\\.(\\s?[A-Za-z])", " . $1").replaceAll("\\s+", " ");
//				newsItem.setNewsSummary(summaryWithoutDuplicates);
				text = text.replaceAll("\\.+", ".").replaceAll("\\.(\\s?[A-Za-z])", " . $1").replaceAll("\\s+", " ");
		        return text;
	}

}
