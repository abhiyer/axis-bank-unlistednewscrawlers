package com.finstinct.pipeline.crawler.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unlisted_news_exceptions")
public class UnlistedNewsExceptions {

	@Id
	@GeneratedValue
	@Column(name = "EXCEPTION_ID")
	private Integer exceptionId;
	
	@Column(name = "EXCEPTION_DATE_TIME")
	private Date exceptionDateTime;
	
	@Column(name = "EXCEPTION_MESSAGE",length = 1000)
	private String exceptionMessage;
	
	@Column(name = "COMPANY_ID")
	private Integer companyId;
	
	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = "SOURCE_ID")
	private Integer sourceId;
	
	@Column(name = "URL",length = 1000)
	private String url;

	public Integer getExceptionId() {
		return exceptionId;
	}

	public void setExceptionId(Integer exceptionId) {
		this.exceptionId = exceptionId;
	}

	public Date getExceptionDateTime() {
		return exceptionDateTime;
	}

	public void setExceptionDateTime(Date exceptionDateTime) {
		this.exceptionDateTime = exceptionDateTime;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "UnlistedNewsExceptions [exceptionId=" + exceptionId + ", exceptionDateTime=" + exceptionDateTime
				+ ", exceptionMessage=" + exceptionMessage + ", companyId=" + companyId + ", companyName=" + companyName
				+ ", sourceId=" + sourceId + ", url=" + url + "]";
	}
	
	
		
}
