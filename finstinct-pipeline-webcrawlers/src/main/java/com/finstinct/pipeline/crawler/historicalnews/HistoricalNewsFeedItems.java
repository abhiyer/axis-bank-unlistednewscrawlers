package com.finstinct.pipeline.crawler.historicalnews;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class HistoricalNewsFeedItems {

	private String title;
	private String description;
	private String link;
	private String author;
	private String url;
	private String linkHtml;
	private Date publishedDate;
	private Integer sourceId;
	private String sourceName;
	private boolean itemAlreadyFetched;
	private String newsSummary;
	private String news;
	private String companyName;
	private Integer companyId;
	private Map<Integer, String> Stocks;
	private String ImageUrl;
	private String code;
	private Integer enterpriseId;
	private String sentiment;
	private String context;
	private String nameFound;
	private String duplicate_ref_id;
	
	public Map<Integer, String> getStocks() {
		return Stocks;
	}

	public void setStocks(Map<Integer, String> stocks2) {
		Stocks = stocks2;
	}

	public String getImageUrl() {
		return ImageUrl;
	}

	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getNews() {
		return news;
	}

	public void setNews(String news) {
		this.news = news;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getLinkHtml() {
		return linkHtml;
	}

	public void setLinkHtml(String linkHtml) {
		this.linkHtml = linkHtml;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public boolean isItemAlreadyFetched() {
		return itemAlreadyFetched;
	}

	public void setItemAlreadyFetched(boolean itemAlreadyFetched) {
		this.itemAlreadyFetched = itemAlreadyFetched;
	}

	public String getNewsSummary() {
		return newsSummary;
	}

	public void setNewsSummary(String newsSummary) {
		this.newsSummary = newsSummary;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer integer) {
		this.enterpriseId = integer;
	}

	public String getNameFound() {
		return nameFound;
	}

	public void setNameFound(String nameFound) {
		this.nameFound = nameFound;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
	
	public String getDuplicate_ref_id() {
		return duplicate_ref_id;
	}

	public void setDuplicate_ref_id(String duplicate_ref_id) {
		this.duplicate_ref_id = duplicate_ref_id;
	}

	@Override
	public String toString() {
		return "HistoricalNewsFeedItems [title=" + title + "]";
	}

}
