package com.finstinct.pipeline.crawler.dao;

import java.util.List;

import com.finstinct.pipeline.crawler.domain.UnlistedCompaniesSubscription;

public interface UnlistedCompaniesSubscriptionDAO extends BaseDAO<UnlistedCompaniesSubscription> {

	void saveSubscriptionData(UnlistedCompaniesSubscription unlistedCompaniesSubscription);

	List<UnlistedCompaniesSubscription> findAlreadysavedInSubScriberTable(Integer companyId, Integer enterpriseId);

	void saveAddedCompanyAsSubScriber(UnlistedCompaniesSubscription unlistedCompaniesSubscription);

	List<UnlistedCompaniesSubscription> findSubscribersByCompanyId(Integer companyId);

	List<UnlistedCompaniesSubscription> findSubscriberOfEnterpriseForCompany(Integer companyId, Integer enterpriseId);

	
}
