package com.finstinct.pipeline.crawler.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="historical_news_item")
public class HistoricalNewsItem {

	@Id
	@GeneratedValue
	@Column(name = "ITEM_ID")
	private Integer itemId;
	
	@Column(name = "ENTITY_ID")
	private Integer entityId;
	
	@Column(name = "ENTERPRISE_ID")
	private Integer enterpriseId;

	@Column(name = "PUBLISH_DATE")
	private Date publishDate;
	
	@Column(name = "ITEM_DATE")
	private Date itemDate;

	@Column(name = "SOURCE_ID")
	private Integer sourceId;

	@Column(name = "PUBLICATION_ID")
	private Integer publicationId;

	@Column(name = "TRIGGERED_DOC_REF")
	private String triggeredDocRef;

	@Column(name = "HISTORICAL_NEWS_DOC_REF")
	private String historicalNewsDocRef;
	
	@Column(name = "TOTAL_POSITIVE")
	private Integer totalPositive;

	@Column(name = "TOTAL_NEGATIVE")
	private Integer totalNegative;

	@Column(name = "TOTAL_NEUTRAL")
	private Integer totalNeutral;
	
	@Column(name = "UPDATE_TIME")
	private Date updateTime;
	
	@Column(name = "URL")
	private String url;

	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "DUPLICATE_REFRENCE_ID")
	private String duplicateReferenceId;
	
	@Column(name = "IsEmailSent")
	private Boolean isEmailSent;
	
	@Column(name = "HandoffLog")
	private String handoffLog;
	
	@Column(name = "NAME_FOUND")
	private String nameFound;
	
	@Column(name = "ITEM_DATA_OBJECT")
	private String itemDataObject;
	
	
	@OneToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="HISTORICAL_NEWS_ITEM_ID")
	private Set <HistoricalCustomizedSummaryReport> historicalCustomizedSummaryReport;
	

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	
	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}
	
	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	
	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public Date getItemDate() {
		return itemDate;
	}

	public void setItemDate(Date itemDate) {
		this.itemDate = itemDate;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getPublicationId() {
		return publicationId;
	}

	public void setPublicationId(Integer publicationId) {
		this.publicationId = publicationId;
	}

	public Integer getTotalPositive() {
		return totalPositive;
	}

	public void setTotalPositive(Integer totalPositive) {
		this.totalPositive = totalPositive;
	}

	public Integer getTotalNegative() {
		return totalNegative;
	}

	public void setTotalNegative(Integer totalNegative) {
		this.totalNegative = totalNegative;
	}

	public Integer getTotalNeutral() {
		return totalNeutral;
	}

	public void setTotalNeutral(Integer totalNeutral) {
		this.totalNeutral = totalNeutral;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDuplicateReferenceId() {
		return duplicateReferenceId;
	}

	public void setDuplicateReferenceId(String duplicateReferenceId) {
		this.duplicateReferenceId = duplicateReferenceId;
	}
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	

	public String getTriggeredDocRef() {
		return triggeredDocRef;
	}

	public void setTriggeredDocRef(String triggeredDocRef) {
		this.triggeredDocRef = triggeredDocRef;
	}

	public String getHistoricalNewsDocRef() {
		return historicalNewsDocRef;
	}

	public void setHistoricalNewsDocRef(String historicalNewsDocRef) {
		this.historicalNewsDocRef = historicalNewsDocRef;
	}

	public Boolean getIsEmailSent() {
		return isEmailSent;
	}

	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}

	public String getHandoffLog() {
		return handoffLog;
	}

	public void setHandoffLog(String handoffLog) {
		this.handoffLog = handoffLog;
	}

	
	public String getNameFound() {
		return nameFound;
	}

	public void setNameFound(String nameFound) {
		this.nameFound = nameFound;
	}

	public String getItemDataObject() {
		return itemDataObject;
	}

	public void setItemDataObject(String itemDataObject) {
		this.itemDataObject = itemDataObject;
	}

	@Override
	public String toString() {
		return "HistoricalNewsItem [itemId=" + itemId + ", entityId=" + entityId + ", enterpriseId=" + enterpriseId
				+ ", publishDate=" + publishDate + ", itemDate=" + itemDate + ", sourceId=" + sourceId
				+ ", publicationId=" + publicationId + ", triggeredDocRef=" + triggeredDocRef
				+ ", historicalNewsDocRef=" + historicalNewsDocRef + ", totalPositive=" + totalPositive
				+ ", totalNegative=" + totalNegative + ", totalNeutral=" + totalNeutral + ", updateTime=" + updateTime
				+ ", url=" + url + ", status=" + status + ", duplicateReferenceId=" + duplicateReferenceId
				+ ", isEmailSent=" + isEmailSent + ", handoffLog=" + handoffLog + ", nameFound=" + nameFound
				+ ", itemDataObject=" + itemDataObject + ", historicalCustomizedSummaryReport="
				+ historicalCustomizedSummaryReport + "]";
	}

	

		  
	
}
