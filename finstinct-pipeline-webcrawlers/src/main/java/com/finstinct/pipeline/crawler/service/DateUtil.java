package com.finstinct.pipeline.crawler.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/** 
 * TODO: This has to move to a proper location/package where utilities are kept
 * 
 *
 *
 */

public class DateUtil {
	
	public static Date resetTo12AM(Date date){		
		LocalDate localDate = new java.sql.Date(date.getTime()).toLocalDate();
	    return java.sql.Date.valueOf(localDate);	
	}
	
	public static Date getPreviousDate(LocalDate startLocalDate, int numDays){		
	    LocalDate endDate = startLocalDate.minusDays(numDays);  
	    return java.sql.Date.valueOf(endDate);	
	}
	
	public static Date getPreviousDate(Date startDate, int numDays){
		LocalDate startLocalDate = new java.sql.Date(startDate.getTime()).toLocalDate();		
	    return getPreviousDate(startLocalDate, numDays);
	}

	public static Date getPreviousDate(Date startDate){
		LocalDate startLocalDate = new java.sql.Date(startDate.getTime()).toLocalDate();		
		LocalDate prevDate = startLocalDate.minusDays(1);  
	    return java.sql.Date.valueOf(prevDate);	
	}

	/**
	 * Returns last n dates starting from today. So for example today is 12th Jan and n is 2 this 
	 * method will return 12th Jan, 11th Jan in the list
	 * @param n
	 * @return
	 */
	public static List<Date> getLastNDates(int n){
		List<Date> dates = new LinkedList<Date>();
		
		Date tomorrow = getTomorrowsDate();
		
		for(int i=1 ; i <= n; i++){
			Date date = getPreviousDate(tomorrow, n);
			dates.add(date);
		}
		return dates;
	}
	
	public static Date getTodaysDate(){
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDate localDate = currentTime.toLocalDate();  
	    return java.sql.Date.valueOf(localDate);
	}
	
	public static Date getTomorrowsDate(){
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDate localDate = currentTime.toLocalDate().plusDays(1);  
	    return java.sql.Date.valueOf(localDate);
	}
	public static Date parseDate(final String date){
		SimpleDateFormat targetDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			return targetDateFormat.parse(date);
		}
		catch (ParseException e){
			e.printStackTrace();
			return new Date();
		}
	}
	
	 public static String getDate(Date s){
	        // Input s = 2016-08-05 14:46:53 +05:30
	        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd").format(s);
			//date = dateFormat.parse(s);
         // System.out.println(date);
			return new SimpleDateFormat("yyyy-MM-dd").format(s).toString();
	        
	    }
		
}
