package com.finstinct.pipeline.crawler.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "enterprise_user_sentiment_context_mapping")
public class EnterpriseUserSentimentContextMapping {

	@Id
	@GeneratedValue
	@Column(name = "ITEM_ID")
	private Integer itemId;

	@Column(name = "ENTERPRISE_ID")
	private Integer enterpriseId;

	@Column(name = "USER_ID")
	private Integer userId;

	@Column(name = "SENTIMENT")
	private String sentiment;
	
    @Column(name = "CONTEXT")
    private String context;
	
    @Column(name = "DATE_CREATED")
	private Date dateCreated;

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public String toString() {
		return "EnterpriseUserSentimentContextMapping [itemId=" + itemId + ", enterpriseId=" + enterpriseId
				+ ", userId=" + userId + ", sentiment=" + sentiment + ", context=" + context + ", dateCreated="
				+ dateCreated + "]";
	}
    
    
        
}
