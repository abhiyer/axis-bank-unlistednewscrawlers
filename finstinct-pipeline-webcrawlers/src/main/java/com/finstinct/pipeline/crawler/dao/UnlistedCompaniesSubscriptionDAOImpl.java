package com.finstinct.pipeline.crawler.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finstinct.pipeline.crawler.domain.UnlistedCompaniesSubscription;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsCompanies;

@Repository
@Transactional
public class UnlistedCompaniesSubscriptionDAOImpl extends BaseDAOImpl<UnlistedCompaniesSubscription> implements UnlistedCompaniesSubscriptionDAO{

	public void saveSubscriptionData(UnlistedCompaniesSubscription unlistedCompaniesSubscription) {
		// TODO Auto-generated method stub

		if (unlistedCompaniesSubscription.getSubscriptionId() == null) {
			save(unlistedCompaniesSubscription);
		} else {
			update(unlistedCompaniesSubscription);
		}
	}

	
	public void saveAddedCompanyAsSubScriber(UnlistedCompaniesSubscription unlistedCompaniesSubscription) {
		// TODO Auto-generated method stub
		if (unlistedCompaniesSubscription.getSubscriptionId() == null) {
			save(unlistedCompaniesSubscription);
		} else {
			update(unlistedCompaniesSubscription);
		}
	}

	
	

	
	public List<UnlistedCompaniesSubscription> findSubscribersByCompanyId(Integer companyId) {

		List<UnlistedCompaniesSubscription> listOfRecords = getEntityManager().createQuery(
				"SELECT en from UnlistedCompaniesSubscription en WHERE en.companyId=:companyId)",
				UnlistedCompaniesSubscription.class).setParameter("companyId",companyId).getResultList();
		
		return listOfRecords;
	}


	
	public List<UnlistedCompaniesSubscription> findAlreadysavedInSubScriberTable(Integer companyId,
			Integer enterpriseId) {
		List<UnlistedCompaniesSubscription> listOfRecords = getEntityManager().createQuery(
				"SELECT en from UnlistedCompaniesSubscription en WHERE en.companyId=:companyId AND en.enterpriseId=:enterpriseId)",
				UnlistedCompaniesSubscription.class).
				setMaxResults(1).
				setParameter("companyId",companyId).setParameter("enterpriseId",enterpriseId).getResultList();
		
		return listOfRecords;
	}


	
	public List<UnlistedCompaniesSubscription> findSubscriberOfEnterpriseForCompany(Integer companyId, Integer enterpriseId) {

		List<UnlistedCompaniesSubscription> listOfRecords = getEntityManager().createQuery(
				"SELECT en from UnlistedCompaniesSubscription en WHERE en.companyId=:companyId AND en.enterpriseId<=:enterpriseId)",
				UnlistedCompaniesSubscription.class).
				setMaxResults(1).
				setParameter("companyId",companyId).setParameter("enterpriseId",enterpriseId).getResultList();
		
		return listOfRecords;
	}



	
	
	
}
