package com.finstinct.pipeline.crawler.dao;

import java.util.List;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSource;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSourceSchedulerConfig;


public interface UnlistedNewsSourcesDAO extends BaseDAO<UnlistedNewsSource>{
	
	List<UnlistedNewsSource> getAllUnlistedNewsSources();
	public String findExistingCompanyName(String companyName, String synonyms);
	public List<UnlistedNewsSource> getnewsSourceForAddCompany(String sourceName);
	String findExistingSourceName(String sourceName);
	void addUpdateNewsSource(UnlistedNewsSource source);
	List<UnlistedNewsSource> findSourceBySourceId(Integer sourceId);
	List<UnlistedNewsSourceSchedulerConfig> getUnlistedNewsSourceSchedulerConfiguration();
}
