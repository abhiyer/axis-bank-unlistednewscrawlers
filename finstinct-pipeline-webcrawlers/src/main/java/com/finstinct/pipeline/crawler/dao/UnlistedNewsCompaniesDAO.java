package com.finstinct.pipeline.crawler.dao;

import java.util.Date;
import java.util.List;

import com.finstinct.pipeline.crawler.domain.EntityNewsItem;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsCompanies;

public interface UnlistedNewsCompaniesDAO extends BaseDAO<UnlistedNewsCompanies> {

	public void saveUnlistedNewsCompanies(UnlistedNewsCompanies unlistedNewsCompanies);

	public List<UnlistedNewsCompanies> getUnlistedNewsCompanies(Integer enterpriseId);

	public String getAlreadyExistsCompanyName(String companyname);

	public String findExistingCompanyName(String companyName);

	public List<UnlistedNewsCompanies> findNewCompanies(Date yesterdaydate, Date todayDate);

	// public List<UnlistedNewsCompanies> findCompanyEmailsData(Integer
	// companyId);

	public UnlistedNewsCompanies findCompanyEmailsData(Integer companyId);

	public List<UnlistedNewsCompanies> findCompanyrecordByCompanyName(String companyName);
	
	public List<UnlistedNewsCompanies> findAlreadySavedCompany(String companyname);

	List<UnlistedNewsCompanies> getCompanyName(String company);

	List<UnlistedNewsCompanies> findUnlistedCompaniesByEnterpriseId(Integer enterpriseId);



}
