package com.finstinct.pipeline.crawler.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Interface BaseDAO.
 * 
 * @param <E> the element type
 */
public interface BaseDAO<E> {

    /**
     * Find by id.
     * 
     * @param pk the pk
     * @return the e
     */
    E findById(Object pk);
    
    /**
     * Find reference by id.
     *
     * @param pk the pk
     * @return the e
     */
    E getReferenceById(Object pk);

    /**
     * Find all.
     * 
     * @return the sets the
     */
    Set<E> findAll();

    /**
     * Refresh object in 1st and 2nd Level caches.
     * 
     * @param pk the pk
     */
    void refresh(Object pk);

    /**
     * Evict entity from 2nd Level cache.
     * 
     * @param pk the pk
     */
    void evict(Object pk);
    
    /**
     * Save.
     * 
     * @param entity the entity
     */
    void save(E entity);

    /**
     * Save collection.
     * 
     * @param collection the collection
     */
    void saveCollection(Collection<E> collection);

    /**
     * Update.
     * 
     * @param entity the entity
     * @return the e
     */
    E update(E entity);
    
    /**
     * Update collection.
     *
     * @param collection the collection
     * @return the collection with updated entities
     */
    Collection<E> updateCollection(Collection<E> collection);

    /**
     * Removes the.
     * 
     * @param pk the pk
     */
    void remove(Object pk);
    
    /**
     * Execute native query.
     */
    List<Object[]> getResultList(String queryStr, Map<String, Object> paramValueMap);
}
