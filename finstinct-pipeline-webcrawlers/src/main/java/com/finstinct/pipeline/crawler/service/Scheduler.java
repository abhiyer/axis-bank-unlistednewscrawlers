package com.finstinct.pipeline.crawler.service;

import com.finstinct.pipeline.crawler.domain.UnlistedNewsSourceSchedulerConfig;

public interface Scheduler {
	
	void pullHistoricalNewsDaily();
	
	void pullHistoricalNewsForAddedCompanies(int current_day_hour, int previous_day_hour, UnlistedNewsSourceSchedulerConfig unlistedNewsSourceSchedulerConfig);
	

}
