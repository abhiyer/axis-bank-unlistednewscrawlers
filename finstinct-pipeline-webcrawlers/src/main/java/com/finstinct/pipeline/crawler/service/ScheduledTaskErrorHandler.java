package com.finstinct.pipeline.crawler.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;

/**
 * Common Error handler for all the scheduled tasks which only logs the error and sends automatic 
 * emails to notify finstinct. Email code to be written.
 *  
 * 
 *
 */
public class ScheduledTaskErrorHandler implements ErrorHandler {
	private static final Logger log = LoggerFactory.getLogger(ScheduledTaskErrorHandler.class);

	public void handleError(Throwable t) {
		log.error("Error in Scheduled task: " + t.getMessage(), t);
	}

	
}
