package com.finstinct.pipeline.crawler.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.hibernate.exception.LockTimeoutException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finstinct.pipeline.crawler.domain.CustomizedSummaryReport;
import com.finstinct.pipeline.crawler.domain.EnterpriseCompanyEmailMapping;
import com.finstinct.pipeline.crawler.domain.EnterpriseUserSentimentContextMapping;
import com.finstinct.pipeline.crawler.domain.EntityNewsItem;
import com.finstinct.pipeline.crawler.domain.HistoricalCustomizedSummaryReport;
import com.finstinct.pipeline.crawler.domain.HistoricalNewsItem;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSource;

@Repository
@Transactional
public class HistoricalNewsItemDAOImpl extends BaseDAOImpl<HistoricalNewsItem> implements HistoricalNewsItemDAO {

	@Override
	public Date findLastUpdatedDate(int sourceId) {
		// TODO Auto-generated method stub
		Date date = null;

		List<HistoricalNewsItem> listOfRecords = getEntityManager().createQuery(
				"SELECT en from HistoricalNewsItem en WHERE en.sourceId=:sourceId ORDER BY en.publishDate desc",
				HistoricalNewsItem.class).setMaxResults(1).setParameter("sourceId", sourceId).getResultList();
		if (listOfRecords != null && !listOfRecords.isEmpty()) {
			date = listOfRecords.get(0).getItemDate();
		}
		return date;

	}

	@Override
	public void addUpdateHistoricalNewsItem(HistoricalNewsItem historicalNewsItem) {
		// TODO Auto-generated method stub
		if (historicalNewsItem.getItemId() == null) {
			save(historicalNewsItem);
		} else {
			update(historicalNewsItem);
		}
	}

	@Override
	public boolean itemExists(String url, Date publishedDate) {

		List<HistoricalNewsItem> item = getEntityManager()
				.createQuery("SELECT en from HistoricalNewsItem en where en.url = :url", HistoricalNewsItem.class)
				.setParameter("url", url).getResultList();
		if (item != null && item.size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public List<HistoricalNewsItem> getHistoricalNews(int entityId, Date startDate) {
		java.sql.Date dateToSql = new java.sql.Date(startDate.getTime());
		return getEntityManager()
				.createQuery("SELECT en from HistoricalNewsItem en where en.entityId = :entityId "
						+ "and en.itemDate = :startDate " + "ORDER BY en.itemId desc", HistoricalNewsItem.class)
				.setParameter("entityId", entityId).setParameter("startDate", dateToSql).getResultList();
	}

	@Override
	public List<Object[]> getUnlistedCustomizedSummaryReportWithCriteria(List<Integer> companyIds, String status,
			Date dateFromSql, Date dateToSql, Integer enterpriseId, Date todaysDate) {

		// TODO Auto-generated method stub
		List<Object[]> results = new LinkedList<Object[]>();

		java.sql.Date dateFrom = new java.sql.Date(dateFromSql.getTime());
		java.sql.Date dateTo = new java.sql.Date(dateToSql.getTime());
		java.sql.Date todaysDateSql = new java.sql.Date(todaysDate.getTime());
		if (status.equalsIgnoreCase("new")) {
			// if status is new take all the data from entity new item table;

			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<Object> query = builder.createQuery();
			Root<HistoricalNewsItem> from = query.from(HistoricalNewsItem.class);
			Subquery<HistoricalCustomizedSummaryReport> subquery = query
					.subquery(HistoricalCustomizedSummaryReport.class);
			Root fromCust = subquery.from(HistoricalCustomizedSummaryReport.class);
			subquery.select(fromCust.get("historicalNewsItemId"));
			Expression<Integer> custenterprise = fromCust.get("enterpriseId");
			Expression<Integer> cuswhereStockIdIn = fromCust.get("entityId");
			Expression<Date> custitemDateExpr = builder.function("DATE", Date.class, fromCust.get("itemDate"));
			// if (isToday) {
			// subquery.where(builder.and(builder.equal(custenterprise,
			// enterpriseId),builder.equal(custitemDateExpr,
			// todaysDateSql),cuswhereStockIdIn.in(stockIds)));
			// }
			if (dateFrom.equals(dateTo)) {
				subquery.where(builder.and(builder.equal(custenterprise, enterpriseId),
						builder.equal(custitemDateExpr, dateFrom), cuswhereStockIdIn.in(companyIds)));
			} else {
				subquery.where(builder.and(builder.equal(custenterprise, enterpriseId),
						builder.greaterThanOrEqualTo(custitemDateExpr, dateFrom),
						builder.lessThanOrEqualTo(custitemDateExpr, dateTo), cuswhereStockIdIn.in(companyIds)));
			}
			subquery.distinct(true);

			Expression<Integer> itemId = from.get("itemId");
			Expression<Integer> entityIdExpr = from.get("entityId");
			Expression<Integer> enterprise = from.get("enterpriseId");
			Expression<Date> itemDateExpr = builder.function("DATE", Date.class, from.get("itemDate"));
			// Expression<String> linkHash = from.get("linkHash");
			Expression<Integer> sourceid = from.get("sourceId");
			Expression<Integer> publicationid = from.get("publicationId");
			Expression<String> triggerDocRefExpr = from.get("triggeredDocRef");
			Expression<String> newsDocRef = from.get("historicalNewsDocRef");
			Expression<Timestamp> updateTimeExpr = builder.function("Timestamp", Timestamp.class,
					from.get("updateTime"));
			Expression<Long> totalPositiveExpr = from.get("totalPositive");
			Expression<Long> totalNeutralExpr = from.get("totalNeutral");
			Expression<Long> totalNegativeExpr = from.get("totalNegative");
			Expression<String> url = from.get("url");
			Expression<String> duplicateReferenceId = from.get("duplicateReferenceId");
			Expression<String> newStatus = from.get("status");
			Expression<Date> publishDateExpr = builder.function("DATE", Date.class, from.get("publishDate"));
			List<Predicate> criteria = new ArrayList<>();
			criteria.add(itemId.in(subquery).not());
			// criteria.add(builder.equal(newStatus, "new"));
			if ((companyIds != null) && (companyIds.size() > 0)) {
				Expression<Integer> whereStockIdIn = from.get("entityId");
				criteria.add(whereStockIdIn.in(companyIds));
			}
			query.multiselect(itemId, entityIdExpr, enterprise, itemDateExpr, sourceid, publicationid,
					triggerDocRefExpr, newsDocRef, updateTimeExpr, totalPositiveExpr, totalNeutralExpr,
					totalNegativeExpr, url, duplicateReferenceId, newStatus, publishDateExpr);

			// if (isToday) {
			// criteria.add(builder.equal(itemDateExpr, todaysDateSql));
			// }
			if (dateFrom.equals(dateTo)) {
				criteria.add(builder.equal(publishDateExpr, dateFrom));
			} else {
				criteria.add(builder.greaterThanOrEqualTo(publishDateExpr, dateFrom));
				criteria.add(builder.lessThanOrEqualTo(publishDateExpr, dateTo));
			}

			Expression<String> enterpriseIdExpr = from.get("enterpriseId");

			// if (mixGeneralNewsWithEnterprise) {
			Predicate eqPredicate = builder.equal(enterpriseIdExpr, enterpriseId);
			Predicate isNullPredicate = builder.isNull(enterpriseIdExpr);
			criteria.add(builder.or(eqPredicate, isNullPredicate));
			// } else {
			// criteria.add(builder.equal(enterpriseIdExpr, enterpriseId));
			// }
			query.where(criteria.toArray(new Predicate[1]));
			// query.groupBy( itemDateExpr);
			query.orderBy(builder.desc(from.get("publishDate")));
			List<Object> resultList = getEntityManager().createQuery(query).getResultList();
			Object[] objArr = null;
			for (Object object : resultList) {
				objArr = (Object[]) object;

				results.add(objArr);
			}
		} else if (status.equalsIgnoreCase("status")) {

			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<Object> query = builder.createQuery();
			Root<HistoricalNewsItem> from = query.from(HistoricalNewsItem.class);
			Join<HistoricalNewsItem, HistoricalCustomizedSummaryReport> cSR = null;

			try {
				cSR = from.join("historicalCustomizedSummaryReport", JoinType.LEFT);
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			Expression<Integer> itemId = from.get("itemId");
			Expression<Integer> entityIdExpr = from.get("entityId");
			Expression<Integer> enterprise = from.get("enterpriseId");
			Expression<Date> itemDateExpr = builder.function("DATE", Date.class, from.get("itemDate"));
			// Expression<String> linkHash = from.get("linkHash");
			Expression<Integer> sourceid = from.get("sourceId");
			Expression<Integer> publicationid = from.get("publicationId");
			Expression<String> triggerDocRefExpr = from.get("triggeredDocRef");
			Expression<String> newsDocRef = from.get("historicalNewsDocRef");
			Expression<Timestamp> updateTimeExpr = builder.function("Timestamp", Timestamp.class,
					from.get("updateTime"));
			Expression<Long> totalPositiveExpr = from.get("totalPositive");
			Expression<Long> totalNeutralExpr = from.get("totalNeutral");
			Expression<Long> totalNegativeExpr = from.get("totalNegative");
			Expression<String> url = from.get("url");
			Expression<String> duplicateReferenceId = from.get("duplicateReferenceId");
			Expression<String> newStatus = from.get("status");
			Expression<Date> publishDateExpr = builder.function("DATE", Date.class, from.get("publishDate"));

			Expression<Integer> citemId = cSR.get("itemId");
			Expression<Integer> centityIdExpr = cSR.get("entityId");
			Expression<Integer> centerprise = cSR.get("enterpriseId");
			Expression<Date> citemDateExpr = builder.function("DATE", Date.class, cSR.get("itemDate"));
			Expression<String> clinkHash = cSR.get("linkHash");
			Expression<Integer> csourceid = cSR.get("sourceId");
			Expression<Integer> cpublicationid = cSR.get("publicationId");
			Expression<String> ccustomizedNewsDocRef = cSR.get("customizedNewsDocRef");
			Expression<Timestamp> cupdateTimeExpr = builder.function("Timestamp", Timestamp.class,
					cSR.get("updateTime"));
			Expression<String> curl = cSR.get("url");
			Expression<String> cnewStatus = cSR.get("status");
			Expression<Integer> centityNewsItemId = cSR.get("historicalNewsItemId");
			Expression<String> cduplicateReferenceId = cSR.get("duplicateReferenceId");

			List<Predicate> criteria = new ArrayList<>();
			// criteria.addAll((Collection<? extends Predicate>)
			// cSR.on(builder.equal(itemId, cSR.get("entityNewsItemId"))));
			if ((companyIds != null) && (companyIds.size() > 0)) {
				Expression<Integer> whereStockIdIn = from.get("entityId");
				criteria.add(whereStockIdIn.in(companyIds));
			}
			query.multiselect(itemId, entityIdExpr, enterprise, itemDateExpr, sourceid, publicationid,
					triggerDocRefExpr, newsDocRef, updateTimeExpr, totalPositiveExpr, totalNeutralExpr,
					totalNegativeExpr, url, duplicateReferenceId, newStatus, publishDateExpr, citemId, centityIdExpr,
					centerprise, citemDateExpr, clinkHash, csourceid, cpublicationid, ccustomizedNewsDocRef,
					cupdateTimeExpr, curl, cnewStatus, centityNewsItemId, cduplicateReferenceId);

			// if (isToday) {
			// criteria.add(builder.equal(itemDateExpr, todaysDateSql));
			// }
			if (dateFrom.equals(dateTo)) {
				criteria.add(builder.equal(publishDateExpr, dateFrom));
			} else {
				criteria.add(builder.greaterThanOrEqualTo(publishDateExpr, dateFrom));
				criteria.add(builder.lessThanOrEqualTo(publishDateExpr, dateTo));
			}

			Expression<String> enterpriseIdExpr = from.get("enterpriseId");
			Expression<String> enterpriseIdCExpr = cSR.get("enterpriseId");
			// if (mixGeneralNewsWithEnterprise) {
			Predicate eqPredicate = builder.equal(enterpriseIdExpr, enterpriseId);
			Predicate isNullPredicate = builder.isNull(enterpriseIdExpr);
			criteria.add(builder.or(eqPredicate, isNullPredicate));
			// } else {
			// criteria.add(builder.equal(enterpriseIdExpr, enterpriseId));
			// }
			// Predicate CeqPredicate = builder.equal(enterpriseIdCExpr,
			// enterpriseId);
			// Predicate CisNullPredicate = builder.isNull(enterpriseIdCExpr);
			// criteria.add(CeqPredicate);
			query.where(criteria.toArray(new Predicate[1]));
			// query.groupBy( itemDateExpr);
			query.orderBy(builder.desc(from.get("publishDate")));
			List<Object> resultList = getEntityManager().createQuery(query).getResultList();
			Object[] objArr = null;
			for (Object object : resultList) {
				objArr = (Object[]) object;

				results.add(objArr);
			}

		} else if (status.equalsIgnoreCase("enable")) {
			// if status is enable take all the data from customized news
			// summary table;
			// @SuppressWarnings("unchecked")
			// List<Object> resultList = getEntityManager()
			// .createQuery("SELECT en from CustomizedSummaryReport en where
			// (en.itemDate < :startDate "
			// + "and en.itemDate >= :endDate) "
			// + "and en.enterpriseId = :enterpriseId "
			// + "and en.entity_id IN ( :stockId ) "
			// +"and en.status = enabled "
			// + "ORDER BY en.update_time desc")
			// .setParameter("stockId", stockIds)
			// .setParameter("startDate", dateFrom)
			// .setParameter("endDate", dateTo)
			// .setParameter("enterpriseId", enterpriseId)
			// .getResultList();
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<Object> query = builder.createQuery();
			Root<HistoricalCustomizedSummaryReport> from = query.from(HistoricalCustomizedSummaryReport.class);

			Expression<Integer> itemId = from.get("itemId");
			Expression<Integer> entityIdExpr = from.get("entityId");
			Expression<Integer> enterprise = from.get("enterpriseId");
			Expression<Date> itemDateExpr = builder.function("DATE", Date.class, from.get("itemDate"));
			Expression<String> linkHash = from.get("linkHash");
			Expression<Integer> sourceid = from.get("sourceId");
			Expression<Integer> publicationid = from.get("publicationId");
			Expression<String> customizedNewsDocRef = from.get("customizedNewsDocRef");
			Expression<Timestamp> updateTimeExpr = builder.function("Timestamp", Timestamp.class,
					from.get("updateTime"));
			Expression<String> url = from.get("url");
			Expression<String> newStatus = from.get("status");
			Expression<Integer> entityNewsItemId = from.get("historicalNewsItemId");
			Expression<String> duplicateReferenceId = from.get("duplicateReferenceId");

			List<Predicate> criteria = new ArrayList<>();
			if ((companyIds != null) && (companyIds.size() > 0)) {
				Expression<Integer> whereStockIdIn = from.get("entityId");
				criteria.add(whereStockIdIn.in(companyIds));
			}
			query.multiselect(itemId, entityIdExpr, enterprise, itemDateExpr, linkHash, sourceid, publicationid,
					customizedNewsDocRef, updateTimeExpr, url, newStatus, entityNewsItemId, duplicateReferenceId);

			// if (isToday) {
			// criteria.add(builder.equal(itemDateExpr, todaysDateSql));
			// }
			if (dateFrom.equals(dateTo)) {
				criteria.add(builder.equal(itemDateExpr, dateFrom));
			} else {
				criteria.add(builder.greaterThanOrEqualTo(itemDateExpr, dateFrom));
				criteria.add(builder.lessThanOrEqualTo(itemDateExpr, dateTo));
			}
			criteria.add(builder.equal(newStatus, "enable"));
			Expression<String> enterpriseIdExpr = from.get("enterpriseId");

			// if (mixGeneralNewsWithEnterprise) {
			Predicate eqPredicate = builder.equal(enterpriseIdExpr, enterpriseId);
			Predicate isNullPredicate = builder.isNull(enterpriseIdExpr);
			criteria.add(builder.or(eqPredicate, isNullPredicate));
			// } else {
			// criteria.add(builder.equal(enterpriseIdExpr, enterpriseId));
			// }
			query.where(criteria.toArray(new Predicate[1]));
			// query.groupBy( itemDateExpr);
			query.orderBy(builder.desc(from.get("itemDate")));
			List<Object> resultList = getEntityManager().createQuery(query).getResultList();
			Object[] objArr = null;
			for (Object object : resultList) {
				objArr = (Object[]) object;

				results.add(objArr);
			}
		} else {
			// if status is disabled take all the data from customized news
			// summary table for disabled summary;
			// @SuppressWarnings("unchecked")
			// List<Object> resultList = getEntityManager()
			// .createQuery("SELECT en from CustomizedSummaryReport en where
			// (en.itemDate < :startDate "
			// + "and en.itemDate >= :endDate) "
			// + "and en.enterpriseId = :enterpriseId "
			// + "and en.entity_id IN ( :stockId ) "
			// +"and en.status = disabled "
			// + "ORDER BY en.update_time desc")
			// .setParameter("stockId", stockIds).setParameter("startDate",
			// dateFrom)
			// .setParameter("endDate", dateTo).setParameter("enterpriseId",
			// enterpriseId).getResultList();

			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<Object> query = builder.createQuery();
			Root<HistoricalCustomizedSummaryReport> from = query.from(HistoricalCustomizedSummaryReport.class);

			Expression<Integer> itemId = from.get("itemId");
			Expression<Integer> entityIdExpr = from.get("entityId");
			Expression<Integer> enterprise = from.get("enterpriseId");
			Expression<Date> itemDateExpr = builder.function("DATE", Date.class, from.get("itemDate"));
			Expression<String> linkHash = from.get("linkHash");
			Expression<Integer> sourceid = from.get("sourceId");
			Expression<Integer> publicationid = from.get("publicationId");
			Expression<String> customizedNewsDocRef = from.get("customizedNewsDocRef");
			Expression<Timestamp> updateTimeExpr = builder.function("Timestamp", Timestamp.class,
					from.get("updateTime"));
			Expression<String> url = from.get("url");
			Expression<String> newStatus = from.get("status");
			Expression<Integer> entityNewsItemId = from.get("historicalNewsItemId");
			Expression<String> duplicateReferenceId = from.get("duplicateReferenceId");

			List<Predicate> criteria = new ArrayList<>();
			if ((companyIds != null) && (companyIds.size() > 0)) {
				Expression<Integer> whereStockIdIn = from.get("entityId");
				criteria.add(whereStockIdIn.in(companyIds));
			}
			query.multiselect(itemId, entityIdExpr, enterprise, itemDateExpr, linkHash, sourceid, publicationid,
					customizedNewsDocRef, updateTimeExpr, url, newStatus, entityNewsItemId, duplicateReferenceId);

			// if (isToday) {
			// criteria.add(builder.equal(itemDateExpr, todaysDateSql));
			// } else
			if (dateFrom.equals(dateTo)) {
				criteria.add(builder.equal(itemDateExpr, dateFrom));
			} else {
				criteria.add(builder.greaterThanOrEqualTo(itemDateExpr, dateFrom));
				criteria.add(builder.lessThanOrEqualTo(itemDateExpr, dateTo));
			}
			criteria.add(builder.equal(newStatus, "disable"));
			Expression<String> enterpriseIdExpr = from.get("enterpriseId");

			// if (mixGeneralNewsWithEnterprise) {
			Predicate eqPredicate = builder.equal(enterpriseIdExpr, enterpriseId);
			Predicate isNullPredicate = builder.isNull(enterpriseIdExpr);
			criteria.add(builder.or(eqPredicate, isNullPredicate));
			// } else {
			// criteria.add(builder.equal(enterpriseIdExpr, enterpriseId));
			// }
			query.where(criteria.toArray(new Predicate[1]));
			// query.groupBy( itemDateExpr);
			query.orderBy(builder.desc(from.get("itemDate")));
			List<Object> resultList = getEntityManager().createQuery(query).getResultList();
			Object[] objArr = null;
			for (Object object : resultList) {
				objArr = (Object[]) object;

				results.add(objArr);
			}
		}

		return results;
	}

	@Override
	public List<EnterpriseCompanyEmailMapping> getEmailsForComapny(Integer companyId, String ComapnyName) {
		List<EnterpriseCompanyEmailMapping> listOfRecords = getEntityManager()
				.createQuery("SELECT en from EnterpriseCompanyEmailMapping en WHERE en.companyId = :companyId",
						EnterpriseCompanyEmailMapping.class)
				.setParameter("companyId", companyId).getResultList();
		return listOfRecords;
	}

	@Override
	public boolean sentimentContextMappingExists(int enterpriseId, String sentiment, String context) {
		List<EnterpriseUserSentimentContextMapping> item = getEntityManager()
				.createQuery(
						"SELECT en from EnterpriseUserSentimentContextMapping en where en.enterpriseId = :enterpriseId and en.context = :context and en.sentiment = :sentiment",
						EnterpriseUserSentimentContextMapping.class)
				.setParameter("enterpriseId", enterpriseId).setParameter("sentiment", sentiment)
				.setParameter("context", context).getResultList();
		if (item != null && item.size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public List<EnterpriseCompanyEmailMapping> getEmailsForComapnyForEnterprise(int enterpriseId) {
		List<EnterpriseCompanyEmailMapping> listOfRecords = getEntityManager()
				.createQuery("SELECT en from EnterpriseCompanyEmailMapping en WHERE en.enterpriseId = :enterpriseId",
						EnterpriseCompanyEmailMapping.class)
				.setParameter("enterpriseId", enterpriseId).getResultList();
		return listOfRecords;
	}

	@Override
	public List<EnterpriseUserSentimentContextMapping> getsentimentContextForEnterprise(int enterpriseId) {
		List<EnterpriseUserSentimentContextMapping> listOfRecords = getEntityManager()
				.createQuery(
						"SELECT en from EnterpriseUserSentimentContextMapping en WHERE en.enterpriseId = :enterpriseId",
						EnterpriseUserSentimentContextMapping.class)
				.setParameter("enterpriseId", enterpriseId).getResultList();
		return listOfRecords;
	}

	@Override
	public String getMaxDateForCompanyRecord(UnlistedNewsSource newsSource) {
		Date date = null;

		List<HistoricalNewsItem> listOfRecords = getEntityManager()
				.createQuery(
						"SELECT en from HistoricalNewsItem en WHERE en.sourceId=:sourceId and en.publishDate = (SELECT max(en.publishDate) from HistoricalNewsItem en WHERE en.sourceId=:sourceId)",
						HistoricalNewsItem.class)
				.setMaxResults(1).setParameter("sourceId", newsSource.getSourceId()).getResultList();
		if (listOfRecords != null && !listOfRecords.isEmpty()) {
			date = listOfRecords.get(0).getPublishDate();
		}
		return date.toString();
	}

	@Override
	public String getMinDateForComapnyRecord(UnlistedNewsSource newsSource) {
		Date date = null;

		List<HistoricalNewsItem> listOfRecords = getEntityManager()
				.createQuery(
						"SELECT en from HistoricalNewsItem en WHERE en.sourceId=:sourceId and en.publishDate = (SELECT min(en.publishDate) from HistoricalNewsItem en WHERE en.sourceId=:sourceId)",
						HistoricalNewsItem.class)
				.setMaxResults(1).setParameter("sourceId", newsSource.getSourceId()).getResultList();
		if (listOfRecords != null && !listOfRecords.isEmpty()) {
			date = listOfRecords.get(0).getPublishDate();
		}
		return date.toString();
	}

	@Override
	public List<HistoricalNewsItem> getHistoricalNews() {
		return getEntityManager().createQuery(
				"SELECT en from HistoricalNewsItem en WHERE en.itemDate BETWEEN '2015-07-19' AND '2019-05-20' ORDER BY en.itemId asc",
				HistoricalNewsItem.class).getResultList();
	}

	@Override
	public List<HistoricalNewsItem> findNewsRecordsByEntityId(Integer entityId) {
		// TODO Auto-generated method stub
		return getEntityManager().createQuery("SELECT en from HistoricalNewsItem en where en.entityId = :entityId ",
				HistoricalNewsItem.class).setParameter("entityId", entityId).getResultList();

	}

	@Override
	public List<HistoricalNewsItem> findNewlyStoredNewsItems(Integer sourceId) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.add(Calendar.HOUR_OF_DAY, 0);
		Date currentDateTime = calendar1.getTime();
		System.out.println(currentDateTime);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -24);
		Date previousDateTime = calendar.getTime();

		boolean isEmailSent = false;
		return getEntityManager()
				.createQuery(
						"SELECT en from HistoricalNewsItem en where en.sourceId = :sourceId and en.isEmailSent = :isEmailSent and en.itemDate>=:previousDateTime and en.itemDate<=:currentDateTime",
						HistoricalNewsItem.class)
				.setParameter("sourceId", sourceId).setParameter("isEmailSent", isEmailSent)
				.setParameter("previousDateTime", previousDateTime).setParameter("currentDateTime", currentDateTime)
				.getResultList();
	}

	@Override
	public List<HistoricalNewsItem> findPendingHandoffNews() {
		String handoffLog = "pending";
		return getEntityManager().createQuery("SELECT en from HistoricalNewsItem en where en.handoffLog = :handoffLog",
				HistoricalNewsItem.class).setParameter("handoffLog", handoffLog).getResultList();
	}

	@Override
	public List<HistoricalNewsItem> findHistoricalNewsRecordsBasedOnEntityAndSource(Integer sourceId,
			Integer entityId) {

		return getEntityManager()
				.createQuery("SELECT en from HistoricalNewsItem en where en.entityId = :entityId "
						+ "and en.sourceId = :sourceId " + "ORDER BY en.itemId desc", HistoricalNewsItem.class)
				.setParameter("entityId", entityId).setParameter("sourceId", sourceId).getResultList();
	}

	@Override
	public List<HistoricalNewsItem> getNewsFetchedToday() {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.add(Calendar.HOUR_OF_DAY, 0);
		Date currentDateTime = calendar1.getTime();
		System.out.println(currentDateTime);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -24);
		Date previousDateTime = calendar.getTime();
		boolean isEmailSent = false;
		
		return getEntityManager()
				.createQuery(
						"SELECT en from HistoricalNewsItem en where en.isEmailSent = :isEmailSent AND en.itemDate>=:previousDateTime AND en.itemDate<=:currentDateTime ORDER BY en.entityId",
						HistoricalNewsItem.class)
				.setParameter("isEmailSent", isEmailSent).setParameter("previousDateTime", previousDateTime)
				.setParameter("currentDateTime", currentDateTime).getResultList();
	}

	@Override
	public List<HistoricalNewsItem> getNewsFetchedTodayForEntity(Integer entityId) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.add(Calendar.HOUR_OF_DAY, 0);
		Date currentDateTime = calendar1.getTime();
		System.out.println(currentDateTime);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -24);
		Date previousDateTime = calendar.getTime();

		boolean isEmailSent = false;
		return getEntityManager()
				.createQuery(
						"SELECT en from HistoricalNewsItem en where en.entityId = :entityId and en.isEmailSent = :isEmailSent and en.itemDate>=:previousDateTime and en.itemDate<=:currentDateTime",
						HistoricalNewsItem.class)
				.setParameter("entityId", entityId).setParameter("isEmailSent", isEmailSent)
				.setParameter("previousDateTime", previousDateTime).setParameter("currentDateTime", currentDateTime)
				.getResultList();
	}

	@Override
	public List<HistoricalNewsItem> getNewsForSourceAndNewCompanyHasNotSentEmail(Integer sourceId, Integer entityId, Date currentDateTime, Date previousDateTime) {
		
		boolean isEmailSent = false;
		return getEntityManager()
				.createQuery(
						"SELECT en from HistoricalNewsItem en where en.isEmailSent = :isEmailSent AND en.itemDate>=:previousDateTime AND en.itemDate<=:currentDateTime AND en.sourceId = :sourceId AND en.entityId = :entityId",
						HistoricalNewsItem.class)
				.setParameter("isEmailSent", isEmailSent).setParameter("previousDateTime", previousDateTime)
				.setParameter("currentDateTime", currentDateTime).setParameter("sourceId", sourceId)
				.setParameter("entityId", entityId)
				.getResultList();
	}
}
