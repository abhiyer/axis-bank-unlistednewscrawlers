package com.finstinct.pipeline.crawler.dao;

import com.finstinct.pipeline.crawler.domain.UnlistedNewsExceptions;

public interface UnlistedExceptionsDAO extends BaseDAO<UnlistedNewsExceptions>{

	void saveUnlistedExceptions(UnlistedNewsExceptions unlistedNewsExceptions);

}
