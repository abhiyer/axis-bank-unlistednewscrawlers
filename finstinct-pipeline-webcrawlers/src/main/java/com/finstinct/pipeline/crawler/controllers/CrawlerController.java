package com.finstinct.pipeline.crawler.controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class CrawlerController {

	private static final Logger LOG = LoggerFactory.getLogger(CrawlerController.class);
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String loadLandingPage(HttpServletRequest request, Model model) {
		
		
		return "redirect:FinstinctCrawlersLandingPage";
	}
	
	
	
	
}
