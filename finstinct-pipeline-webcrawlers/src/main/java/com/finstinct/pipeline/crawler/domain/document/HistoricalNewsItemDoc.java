package com.finstinct.pipeline.crawler.domain.document;

import java.util.Date;

public class HistoricalNewsItemDoc {
	  private String title;
      private String description;
      private String news;
      private String link;
      private String author;
      private String uri;
      private Date publishedDate;
      private String sourceName;
      private String stockName;
      private String stockCode;
      private String url;
      private String imageUrl;
      
    private long totalPositive;

  	private long totalNeutral;

  	private long totalNegative;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public long getTotalPositive() {
		return totalPositive;
	}

	public void setTotalPositive(long totalPositive) {
		this.totalPositive = totalPositive;
	}

	public long getTotalNeutral() {
		return totalNeutral;
	}

	public void setTotalNeutral(long totalNeutral) {
		this.totalNeutral = totalNeutral;
	}

	public long getTotalNegative() {
		return totalNegative;
	}

	public void setTotalNegative(long totalNegative) {
		this.totalNegative = totalNegative;
	}
   
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	

	public String getNews() {
		return news;
	}

	public void setNews(String news) {
		this.news = news;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "HistoricalNewsItemDoc [title=" + title + ", description=" + description + ", news=" + news + ", link="
				+ link + ", author=" + author + ", uri=" + uri + ", publishedDate=" + publishedDate + ", sourceName="
				+ sourceName + ", stockName=" + stockName + ", stockCode=" + stockCode + ", url=" + url + ", imageUrl="
				+ imageUrl + ", totalPositive=" + totalPositive + ", totalNeutral=" + totalNeutral + ", totalNegative="
				+ totalNegative + "]";
	}

	

	
}
