package com.finstinct.pipeline.crawler.events;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multiset.Entry;


/**
 * @author 
 *
 */
public class Util {

	/*
	 * public static final List<String> getProbableCompanyNames(String sAll) {
	 * if (sAll == null) return null; List<String> probables = new
	 * ArrayList<String>(); for (String s: sAll.split( ",", -1)) { List<String>
	 * retList = new ArrayList(Arrays.asList(removeExtras(s).split( "\\s+",
	 * -1)));
	 * 
	 * String probable = ""; int count = 0; int totalCount = retList.size(); for
	 * (String r: retList) { count ++; //System.out.println("Investigating for "
	 * + r); String first = r.substring(0, 1);
	 * //System.out.println("Checking first " + first); String upperfirst =
	 * first.toUpperCase(); if (upperfirst.equals(first)) { probable += " " + r;
	 * //System.out.println("prbable is : " + probable); } else if
	 * (!probable.equals("")) { probables.add(probable); probable = ""; } if
	 * (count == totalCount) probables.add(probable); } } return probables; }
	 */
	public static String[] FORWARDLOOKINGSTATEMENTKEYARRAY = new String[] { "likely", "revised", "expect", "plans to", "target",
			"will lead to", "going forward", "pipeline", "we see", "continues", "indications", "estimate" };

	public static final List<String> getProbableCompanyNames(String sAll) {
		if (sAll == null)
			return null;
		List<String> probables = new ArrayList<String>();
		for (String s : sAll.split(",", -1)) {
			List<String> retList = Arrays.asList(removeExtras(s).split("\\s+", -1));

			int count = 0;
			int totalCount = retList.size();
			for (String r : retList) {
				count++;
				String probable = "";

				// System.out.println("Investigating for " + r);
				// String first = r.substring(0, 1);
				// String upperfirst = first.toUpperCase();
				List<String> all = new ArrayList<String>();
				// if (upperfirst.equals(first)) {
				all.add(r);
				for (String r1 : all) {
					if (r1 != null && !r1.isEmpty()) {
						String first = r1.substring(0, 1);
						String upperfirst = first.toUpperCase();
						if (first.equals(upperfirst)) {
							probables.add(r1);
							// System.out.println("adding  " + r);
							probable = r1;
							for (int i = count; i < totalCount; ++i) {
								probable += " " + retList.get(i);
								probables.add(probable.trim());
								// System.out.println("adding  " + probable);
							}
						}
					}
				}
			}
		}
		List<String> apos = new ArrayList<String>();
		for (String p : probables) {
			if (p.contains("\'s")) {
				apos.add(p.replace("\'", ""));
				apos.add(p.replace("\'s", ""));
			} else if (p.contains("’s")) {
				apos.add(p.replace("’", ""));
				apos.add(p.replace("’s", ""));
			}
		}
		probables.addAll(apos);
		// probables.sort(new StringLengthComparator());
		return probables;
	}

	

	
	public static String removeExtras(String title) {
		if (title != null) {
			title = title.trim();
			title = title.replaceAll("\"", "");
			// title = title.replaceAll("\'s ", "");
			// title = title.replaceAll("\'", "");
			title = title.replaceAll(":", "");
			// title = title.replaceAll(",", "");
			title = title.replaceAll(";", "");
			// title = title.replaceAll(":", "");
			title = title.replaceAll("-", " ");
			title = title.replaceAll("%", "");
			title = title.replaceAll("’s", " ");
			title = title.replaceAll("’", " ");
			title = title.replaceAll("‘", "");

			// title = title.replaceAll("?", "");
		}
		return title;

	}


	/**
	 * 
	 * @param stockName
	 * @param stockList
	 * @return
	 */
	public static String IsStockPresent(String stockName,
			ListMultimap<String, String> stockList) {
		String result = "";

//		if (stockList.containsValue(stockName.toLowerCase())) {
//			// System.out.println("seems like value exists " + stockName);
//			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
//					stockList, ArrayListMultimap.<String, String> create());
//
//			Collection<String> resultList = invertedMultimap.get(stockName
//					.toLowerCase());
//
//			if (resultList.size() > 0) {
//				return resultList.toArray()[0].toString();
//			}
//		}

		if (stockList.containsValue(stockName.toLowerCase().trim())) {
			// System.out.println("seems like value exists " + stockName);
			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
					stockList, ArrayListMultimap.<String, String> create());

			Collection<String> resultList = invertedMultimap.get(stockName
					.toLowerCase());

			if (resultList.size() > 0) {
				return resultList.toArray()[0].toString();
			}
		}else if(stockName.toLowerCase().contains(".")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace(".", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace(".", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace(".", "").trim());

				if (resultList.size() > 0) {
					return resultList.toArray()[0].toString();
				}
			}
		}else if(stockName.toLowerCase().contains(",")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace(",", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace(",", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace(",", "").trim());

				if (resultList.size() > 0) {
					return resultList.toArray()[0].toString();
				}
			}
		}else if(stockName.toLowerCase().contains("'s")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace("'s", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace("'s", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace("'s", "").trim());

				if (resultList.size() > 0) {
					return resultList.toArray()[0].toString();
				}
			}
		}else if(stockName.toLowerCase().contains("'")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace("'", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace("'", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace("'", "").trim());

				if (resultList.size() > 0) {
					return resultList.toArray()[0].toString();
				}
			}
		}
		return result;
	}

	/**
	 * Get all the stocks found in the text
	 * Returns a map of <StockID, Description>
	 * 
	 * @param probables
	 * @param stockList
	 * @return
	 */
	public static Map<Integer, String> getStocksPresent(List<String> probables,
			ListMultimap<String, String> stockList) {
		List<Integer> sids = new LinkedList<Integer>();
		//System.err.println("Searching for stocks:" + Util.join(stockList.values(), ",", true));
		//System.err.flush();
		Map<Integer, String> resultList = new HashMap<Integer, String>();
		Map<Integer, String> finalResultList = new HashMap<Integer, String>();
		for (String p : probables) {
			//String s = IsStockPresent(p, stockList);
			// This IsExactStockPresent is for exact match to stock @author Aditya
			String s = IsExactStockPresent(p, stockList);
			if (s != null && !s.isEmpty()) {
				sids.add(Integer.valueOf(s));
				resultList.put(Integer.valueOf(s), p);
			}
		}
		
		
		// checki8ng foir multiple stocks are correct or not
		
		for(int k=0;k == 0; k++){
			String stockcontain = "single";
			for(int l=0;l<resultList.size();l++){
				//if(resultList.get(sids.get(l)).toLowerCase().indexOf(resultList.get(sids.get(k)).toLowerCase()) != -1){
				if(resultList.get(sids.get(l)).toLowerCase().contains(resultList.get(sids.get(k)).toLowerCase())){
					stockcontain = "single";
				}else{
					stockcontain = "multiple";
					break;
				}
			}
			
			if(stockcontain.equals("single")){
				// From this we will get the perfect match of stock from title of html.
				for(int i=0;i<resultList.size();i++){
					String length = "large";
					for(int j=0;j<resultList.size();j++){
						if(resultList.get(sids.get(i)).length() < resultList.get(sids.get(j)).length()){
							length = "small";
						}
					}
					
					if(length.equals("large")){
						finalResultList.put(Integer.valueOf(sids.get(i)), resultList.get(sids.get(i)));
						break;
					}
				}
			}else{
				finalResultList.putAll(resultList);
			}
		}
		
		
		return finalResultList;
	}
	
	/**
	 * Get the most probable stock found in the text (Longest matching stock)
	 * Return the stock ID as String
	 * @param probables
	 * @param stockList
	 * @return
	 */
	public static String getStockPresent(List<String> probables,
			ListMultimap<String, String> stockList) {
		
		String longest="";
		
		for (String p : probables) {
			String s = IsStockPresent(p, stockList);
			if (s != null && !s.isEmpty()) {
				if(s.length() > longest.length()){
					longest = s;
				}
			}
		}
		return longest;
	}

	/**
	 * Get all the stocks found in the text
	 * Returns a map of <StockID, Description>
	 * 
	 * @param probables
	 * @param stockList
	 * @return
	 */
	public static Map<Integer, String> getComapniesPresent(List<String> probables,
			ListMultimap<String, String> stockList) {
		List<Integer> sids = new LinkedList<Integer>();
		//System.err.println("Searching for stocks:" + Util.join(stockList.values(), ",", true));
		//System.err.flush();
		Map<Integer, String> resultList = new HashMap<Integer, String>();
		Map<Integer, String> finalResultList = new HashMap<Integer, String>();
		for (String p : probables) {
			String s = IsCompaniesPresent(p, stockList);
			if (s != null && !s.isEmpty()) {
				sids.add(Integer.valueOf(s));
				resultList.put(Integer.valueOf(s), p);
			}
		}
		
		
		// checki8ng foir multiple stocks are correct or not
		
		for(int k=0;k == 0; k++){
			String stockcontain = "single";
			for(int l=0;l<resultList.size();l++){
				//if(resultList.get(sids.get(l)).toLowerCase().indexOf(resultList.get(sids.get(k)).toLowerCase()) != -1){
				if(resultList.get(sids.get(l)).toLowerCase().contains(resultList.get(sids.get(k)).toLowerCase())){
					stockcontain = "single";
				}else{
					stockcontain = "multiple";
					break;
				}
			}
			
			if(stockcontain.equals("single")){
				// From this we will get the perfect match of stock from title of html.
				for(int i=0;i<resultList.size();i++){
					String length = "large";
					for(int j=0;j<resultList.size();j++){
						if(resultList.get(sids.get(i)).length() < resultList.get(sids.get(j)).length()){
							length = "small";
						}
					}
					
					if(length.equals("large")){
						finalResultList.put(Integer.valueOf(sids.get(i)), resultList.get(sids.get(i)));
						break;
					}
				}
			}else{
				finalResultList.putAll(resultList);
			}
		}
		
		
		return finalResultList;
	}
	

	/**
	 * 
	 * @param stockName
	 * @param stockList
	 * @return
	 */
	public static String IsCompaniesPresent(String stockName,
			ListMultimap<String, String> stockList) {
		String result = "";

//		if (stockList.containsValue(stockName.toLowerCase())) {
//			// System.out.println("seems like value exists " + stockName);
//			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
//					stockList, ArrayListMultimap.<String, String> create());
//
//			Collection<String> resultList = invertedMultimap.get(stockName
//					.toLowerCase());
//
//			if (resultList.size() > 0) {
//				return resultList.toArray()[0].toString();
//			}
//		}

		 Set keySet = stockList.keySet( );
		    Iterator keyIterator = keySet.iterator();
		    while( keyIterator.hasNext( ) ) {
		        Object key = keyIterator.next( );
		       // System.out.print( "Key: " + key + ", " );
		         
		        Collection values = (Collection) stockList.get( (String) key );
		        Iterator valuesIterator = values.iterator( );
		        while( valuesIterator.hasNext( ) ) {
		            String foundStockName = valuesIterator.next( ).toString().trim();
		         //   System.out.print( "Value: " + foundStockName + ". " );
		            if (foundStockName.equalsIgnoreCase(stockName.toLowerCase().toString().trim())) {
		    			// System.out.println("seems like value exists " + stockName);
		    			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
		    					stockList, ArrayListMultimap.<String, String> create());

		    			Collection<String> resultList = invertedMultimap.get(stockName
		    					.toLowerCase());

		    			if (resultList.size() > 0) {
		    				return resultList.toArray()[0].toString();
		    			}
		    		}
		        }
		      //  System.out.print( "\n" );
		    }

		
		return result;
	}
	
	/**
	 * 
	 * @param stockName
	 * @param stockList
	 * @return
	 */
	public static String IsExactStockPresent(String stockName,
			ListMultimap<String, String> stockList) {
		String result = "";

//		if (stockList.containsValue(stockName.toLowerCase())) {
//			// System.out.println("seems like value exists " + stockName);
//			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
//					stockList, ArrayListMultimap.<String, String> create());
//
//			Collection<String> resultList = invertedMultimap.get(stockName
//					.toLowerCase());
//
//			if (resultList.size() > 0) {
//				return resultList.toArray()[0].toString();
//			}
//		}

		 Set keySet = stockList.keySet( );
		    Iterator keyIterator = keySet.iterator();
		    while( keyIterator.hasNext( ) ) {
		        Object key = keyIterator.next( );
		       // System.out.print( "Key: " + key + ", " );
		         
		        Collection values = (Collection) stockList.get( (String) key );
		        Iterator valuesIterator = values.iterator( );
		        while( valuesIterator.hasNext( ) ) {
		            String foundStockName = valuesIterator.next( ).toString().trim();
		         //   System.out.print( "Value: " + foundStockName + ". " );
		            if (foundStockName.equalsIgnoreCase(stockName.toLowerCase().toString().trim())) {
		    			// System.out.println("seems like value exists " + stockName);
		    			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
		    					stockList, ArrayListMultimap.<String, String> create());

		    			Collection<String> resultList = invertedMultimap.get(stockName
		    					.toLowerCase());

		    			if (resultList.size() > 0) {
		    				return resultList.toArray()[0].toString();
		    			}
		    		}else if(stockName.toLowerCase().contains(".")){
		    			//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
						String n = stockName.toLowerCase().replace(".", "").trim();
						if (foundStockName.equalsIgnoreCase(stockName.toLowerCase().replace(".", "").trim())) {
							// System.out.println("seems like value exists " + stockName);
							Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
									stockList, ArrayListMultimap.<String, String> create());

							Collection<String> resultList = invertedMultimap.get(stockName
									.toLowerCase().replace(".", "").trim());

							if (resultList.size() > 0) {
								return resultList.toArray()[0].toString();
							}
						}
					}else if(stockName.toLowerCase().contains(",")){
					//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
						String n = stockName.toLowerCase().replace(",", "").trim();
						if (foundStockName.equalsIgnoreCase(stockName.toLowerCase().replace(",", "").trim())) {
							// System.out.println("seems like value exists " + stockName);
							Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
									stockList, ArrayListMultimap.<String, String> create());

							Collection<String> resultList = invertedMultimap.get(stockName
									.toLowerCase().replace(",", "").trim());

							if (resultList.size() > 0) {
								return resultList.toArray()[0].toString();
							}
						}
					}else if(stockName.toLowerCase().contains("'s")){
					//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
						String n = stockName.toLowerCase().replace("'s", "").trim();
						if (foundStockName.equalsIgnoreCase(stockName.toLowerCase().replace("'s", "").trim())) {
							// System.out.println("seems like value exists " + stockName);
							Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
									stockList, ArrayListMultimap.<String, String> create());

							Collection<String> resultList = invertedMultimap.get(stockName
									.toLowerCase().replace("'s", "").trim());

							if (resultList.size() > 0) {
								return resultList.toArray()[0].toString();
							}
						}
					}else if(stockName.toLowerCase().contains("'")){
					//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
						String n = stockName.toLowerCase().replace("'", "").trim();
						if (foundStockName.equalsIgnoreCase(stockName.toLowerCase().replace("'", "").trim())) {
							// System.out.println("seems like value exists " + stockName);
							Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
									stockList, ArrayListMultimap.<String, String> create());

							Collection<String> resultList = invertedMultimap.get(stockName
									.toLowerCase().replace("'", "").trim());

							if (resultList.size() > 0) {
								return resultList.toArray()[0].toString();
							}
						}
					}
		        }
		      //  System.out.print( "\n" );
		    }
		    
		return result;
	}
	
	public static Map<Integer, String> getStocksPresentForPromoter(List<String> probables,
			ListMultimap<String, String> stockList) {
		List<Integer> sids = new LinkedList<Integer>();
		//System.err.println("Searching for stocks:" + Util.join(stockList.values(), ",", true));
		//System.err.flush();
		Map<Integer, String> resultList = new HashMap<Integer, String>();
		Map<Integer, String> finalResultList = new HashMap<Integer, String>();
		for (String p : probables) {
		
			String sa = IsPromoterPresent(p, stockList);
			try{
		            for(String s : sa.replace("[", "").replace("]", "").split(",")){
			        if (s != null && !s.isEmpty()) {
		             //	 	sids.add(Integer.valueOf(s));
			    	   resultList.put(Integer.valueOf(s.trim()), p);
			         }
		           }
			
			}catch(Exception e){
		     e.printStackTrace();	
		    }
		}
		
	//	finalResultList.putAll(resultList);
		
		// checki8ng foir multiple stocks are correct or not
		
//		for(int k=0;k == 0; k++){
//			String stockcontain = "single";
//			for(int l=0;l<resultList.size();l++){
//				//if(resultList.get(sids.get(l)).toLowerCase().indexOf(resultList.get(sids.get(k)).toLowerCase()) != -1){
//				if(resultList.get(sids.get(l)).toLowerCase().contains(resultList.get(sids.get(k)).toLowerCase())){
//					stockcontain = "single";
//				}else{
//					stockcontain = "multiple";
//					break;
//				}
//			}
//			
//			if(stockcontain.equals("single")){
//				// From this we will get the perfect match of stock from title of html.
//				for(int i=0;i<resultList.size();i++){
//					String length = "large";
//					for(int j=0;j<resultList.size();j++){
//						if(resultList.get(sids.get(i)).length() < resultList.get(sids.get(j)).length()){
//							length = "small";
//						}
//					}
//					
//					if(length.equals("large")){
//						finalResultList.put(Integer.valueOf(sids.get(i)), resultList.get(sids.get(i)));
//						break;
//					}
//				}
//			}else{
//				finalResultList.putAll(resultList);
//			}
//		}
		
		
		return resultList;
	}
	

public static String IsPromoterPresent(String stockName,
			ListMultimap<String, String> stockList) {
		String result = "";

//		if (stockList.containsValue(stockName.toLowerCase())) {
//			// System.out.println("seems like value exists " + stockName);
//			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
//					stockList, ArrayListMultimap.<String, String> create());
//
//			Collection<String> resultList = invertedMultimap.get(stockName
//					.toLowerCase());
//
//			if (resultList.size() > 0) {
//				return resultList.toArray()[0].toString();
//			}
//		}

		if (stockList.containsValue(stockName.toLowerCase().trim())) {
			// System.out.println("seems like value exists " + stockName);
			Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
					stockList, ArrayListMultimap.<String, String> create());

			Collection<String> resultList = invertedMultimap.get(stockName
					.toLowerCase());

			if (resultList.size() > 0) {
				return resultList.toString();
			}
		}else if(stockName.toLowerCase().contains(".")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace(".", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace(".", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace(".", "").trim());

				if (resultList.size() > 0) {
					return resultList.toString();
				}
			}
		}else if(stockName.toLowerCase().contains(",")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace(",", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace(",", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace(",", "").trim());

				if (resultList.size() > 0) {
					return resultList.toString();
				}
			}
		}else if(stockName.toLowerCase().contains("'s")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace("'s", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace("'s", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace("'s", "").trim());

				if (resultList.size() > 0) {
					return resultList.toString();
				}
			}
		}else if(stockName.toLowerCase().contains("'")){
		//	System.out.println(stockName.toLowerCase().replaceAll(".", "").trim());
			String n = stockName.toLowerCase().replace("'", "").trim();
			if (stockList.containsValue(stockName.toLowerCase().replace("'", "").trim())) {
				// System.out.println("seems like value exists " + stockName);
				Multimap<String, String> invertedMultimap = Multimaps.invertFrom(
						stockList, ArrayListMultimap.<String, String> create());

				Collection<String> resultList = invertedMultimap.get(stockName
						.toLowerCase().replace("'", "").trim());

				if (resultList.size() > 0) {
					return resultList.toString();
				}
			}
		}
		return result;
	}
}
