package com.finstinct.pipeline.crawler.env.processors;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import com.finstinct.pipeline.crawler.dao.UnlistedNewsSourcesDAO;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSourceSchedulerConfig;
import com.finstinct.pipeline.crawler.service.Scheduler;
import com.finstinct.pipeline.crawler.service.SchedulerImpl;
import com.finstinct.pipeline.crawler.service.addCompany.GenericNewsLoader;


@Configuration
@EnableScheduling
public class SchedulerConfig implements SchedulingConfigurer, Scheduler {
	
	
	private static final Logger log = LoggerFactory.getLogger(SchedulerConfig.class);
	
	@Autowired
	private UnlistedNewsSourcesDAO unlistedNewsSourcesDao;
	
	@Autowired
	private GenericNewsLoader genericNewsLoader;
	
	private List<UnlistedNewsSourceSchedulerConfig> newsSourcesConfig = null;
	
	private boolean status = false;
	
	
	
	public List<UnlistedNewsSourceSchedulerConfig> getNewsSourcesConfig() {
		return newsSourcesConfig;
	}


	public void setNewsSourcesConfig(List<UnlistedNewsSourceSchedulerConfig> newsSourcesConfig) {
		
		try {
			newsSourcesConfig = unlistedNewsSourcesDao.getUnlistedNewsSourceSchedulerConfiguration();
			status = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.newsSourcesConfig = newsSourcesConfig;
	}
		

			@Override
			public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
				// TODO Auto-generated method stub
				/*ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
				threadPoolTaskScheduler.setPoolSize(1);
		        threadPoolTaskScheduler.setThreadNamePrefix("scheduled-task-pool-");
		        threadPoolTaskScheduler.initialize();
		        scheduledTaskRegistrar.setTaskScheduler(threadPoolTaskScheduler);*/
		
				/*if(getNewsSourcesConfig() == null) {*/
					setNewsSourcesConfig(new ArrayList<>());
					/*status = true;*/
				/*} else {
			
				}*/
					
				//if(status)
					
					for(UnlistedNewsSourceSchedulerConfig unlistedNewsSourceSchedulerConfig : getNewsSourcesConfig()) {
						
				Runnable runnable = () -> {
					//System.out.println("Trigger task executed at " + new Date() + " :: " + unlistedNewsSourceSchedulerConfig.getSourceName());
					
					if(unlistedNewsSourceSchedulerConfig.getConfigType().equalsIgnoreCase("historical")) {
						System.out.println("Test ########################################### Test");
						System.out.println("thread number ::: " + Thread.currentThread().getId());
						
						pullHistoricalNewsForAddedCompanies(unlistedNewsSourceSchedulerConfig.getCurrentDayHour(), 
								unlistedNewsSourceSchedulerConfig.getPreviousDayHour(), 
								unlistedNewsSourceSchedulerConfig);
					} else if(unlistedNewsSourceSchedulerConfig.getConfigType().equalsIgnoreCase("daily"))
						pullHistoricalNewsDaily();
						
					
					
				};
				
		
				Trigger trigger = new Trigger() {
			
					public Date nextExecutionTime(TriggerContext triggerContext) {
			
			            	CronTrigger crontrigger = new CronTrigger(unlistedNewsSourceSchedulerConfig.getCronConfig());
			
			            	return crontrigger.nextExecutionTime(triggerContext);
			
			            	}
			
			        	};
			        
			        	scheduledTaskRegistrar.addTriggerTask(runnable, trigger);
					}

			}


			public void pullHistoricalNewsDaily() {
				System.out.println("pullHistoricalNewsDaily ran");
				log.debug(" pullHistoricalNewsDaily Scheduling Analyzer...");
				try {
					
				} catch(Exception e ) {
					e.printStackTrace();
				}
			}

			public void pullHistoricalNewsForAddedCompanies(int current_day_hour, int previous_day_hour, UnlistedNewsSourceSchedulerConfig unlistedNewsSourceSchedulerConfig) {
				System.out.println("pullHistoricalNewsForAddedCompaniess ran");
				log.debug(" pullHistoricalNewsForAddedCompanies Scheduling Analyzer...");
				
				try {
					Calendar calendar1 = Calendar.getInstance();
					calendar1.add(Calendar.HOUR_OF_DAY, current_day_hour);
					Date currentDateTime = calendar1.getTime();
					System.out.println("now :: " + currentDateTime);
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.HOUR_OF_DAY, previous_day_hour);
					Date previousDateTime = calendar.getTime();
					System.out.println("prev :: " + previousDateTime);
					
					System.out.println("type ##### " + unlistedNewsSourceSchedulerConfig.getSourceName());
					
					/*List<UnlistedNewsSourceSchedulerConfig> newsSourcesConfig = new ArrayList<>();
					newsSourcesConfig = getNewsSourcesConfig();*/
					
					/*for(UnlistedNewsSourceSchedulerConfig newsSourceConfig : newsSourcesConfig)
					{
						if(newsSourceConfig.getConfigType().equalsIgnoreCase("historical"))
						{*/
							System.out.println(unlistedNewsSourceSchedulerConfig.getSourceName());
							genericNewsLoader.loadGenericNews(unlistedNewsSourceSchedulerConfig, currentDateTime, previousDateTime);
					/*	}
					}*/
				
					//newsSources = unlistedNewsSourcesDao.getnewsSourceForAddCompany("indianexpress");
				
				
					
					
					//System.out.println(unlistedNewsSourcesDao.getnewsSourceForAddCompany("indianexpress"));
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
	
	

}
