package com.finstinct.pipeline.crawler.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finstinct.pipeline.crawler.domain.UnlistedNewsExceptions;



@Repository
@Transactional
public class UnlistedExceptionsDAOImpl extends BaseDAOImpl<UnlistedNewsExceptions> implements UnlistedExceptionsDAO{

	
	public void saveUnlistedExceptions(UnlistedNewsExceptions unlistedNewsExceptions) {
		// TODO Auto-generated method stub
		
		if (unlistedNewsExceptions.getExceptionId() == null) {
			save(unlistedNewsExceptions);
		} else {
			update(unlistedNewsExceptions);
		}
	}
	

}
