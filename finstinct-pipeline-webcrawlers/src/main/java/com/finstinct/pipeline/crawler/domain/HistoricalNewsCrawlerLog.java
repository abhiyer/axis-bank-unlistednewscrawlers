package com.finstinct.pipeline.crawler.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="historical_news_crawler_log")
public class HistoricalNewsCrawlerLog {

	@Id
	@GeneratedValue
	@Column(name = "historical_news_crawler_log_id")
	private Integer historicalNewsCrawlerLogId;
	
	@Column(name = "company_id")
	private Integer companyId;
	
	@Column(name = "start_date")
	private Date startDate;
	
	@Column(name = "end_date")
	private Date endDate;
	
	@Column(name = "source_id")
	private Integer sourceId;

	public Integer getHistoricalNewsCrawlerLogId() {
		return historicalNewsCrawlerLogId;
	}

	public void setHistoricalNewsCrawlerLogId(Integer historicalNewsCrawlerLogId) {
		this.historicalNewsCrawlerLogId = historicalNewsCrawlerLogId;
	}

	



	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	@Override
	public String toString() {
		return "HistoricalNewsCrawlerLog [historicalNewsCrawlerLogId=" + historicalNewsCrawlerLogId + ", companyId="
				+ companyId + ", startDate=" + startDate + ", endDate=" + endDate + ", sourceId=" + sourceId + "]";
	}

	
	
	
	
	}
