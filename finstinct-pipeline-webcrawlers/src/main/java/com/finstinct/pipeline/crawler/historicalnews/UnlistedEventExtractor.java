package com.finstinct.pipeline.crawler.historicalnews;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.finstinct.pipeline.crawler.events.Util;
import com.google.common.collect.ListMultimap;


@Component
public class UnlistedEventExtractor {
	private static final Logger LOG = LoggerFactory.getLogger(UnlistedEventExtractor.class);
	
	
	private ListMultimap<String, String> companyList = null;
	
	public ListMultimap<String, String> getCompanyList() {
		return companyList;
	}
	public void setCompanyList(ListMultimap<String, String> companyList) {
		this.companyList = companyList;
	}
	
public Map<Integer, String> extractCompanyUnlisted(final String title, final String news,ListMultimap<String, String> companyListWithSynonyms) throws Exception {
		
		List<String> probable = new LinkedList<String>();
		
		//Ideally there should be one
		//List<String> titlesInHtml = findTitle(news);
		 
		if (title.length() > 3 ){
			probable.addAll(Util.getProbableCompanyNames(title));
		}
		if(news.length() > 0){
			probable.addAll(Util.getProbableCompanyNames(news));
		}
//		for(String titleInHtml: titlesInHtml){
//			probable.addAll(Util.getProbableCompanyNames(titleInHtml));
//		}
		 
		Map<Integer, String> companies = Util.getStocksPresent(probable,companyListWithSynonyms);
		
		
		//LOG.debug("Found following companies in the feed {}", Util.join(companies.values(), ";",false));
		
		return companies;		
	}
	
}
