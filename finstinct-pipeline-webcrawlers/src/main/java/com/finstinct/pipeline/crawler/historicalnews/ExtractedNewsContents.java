package com.finstinct.pipeline.crawler.historicalnews;

import java.util.Date;

public class ExtractedNewsContents {
	
	private Date publishedDate;
	
	private String newsHeadline;
	
	private String newsBody;

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getNewsHeadline() {
		return newsHeadline;
	}

	public void setNewsHeadline(String newsHeadline) {
		this.newsHeadline = newsHeadline;
	}

	public String getNewsBody() {
		return newsBody;
	}

	public void setNewsBody(String newsBody) {
		this.newsBody = newsBody;
	}
	
	

}
