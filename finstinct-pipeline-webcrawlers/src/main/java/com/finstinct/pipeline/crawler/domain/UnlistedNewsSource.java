package com.finstinct.pipeline.crawler.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unlisted_news_source")
public class UnlistedNewsSource {
	
	@Id
	@GeneratedValue
	@Column(name = "SOURCE_ID")
	private Integer sourceId;

	@Column(name = "SOURCE_NAME")
	private String sourceName;
	
	@Column(name = "API_TYPE")
	private String apiType;

	@Column(name = "URL")
	private String url;
	
	@Column(name = "SEARCH_URL")
	private String searchUrl;
	
	@Column(name = "LIST_TAG")
	private String listTag;

	@Column(name = "NEWS_URL")
	private String newsUrl;
	
	@Column(name = "PUBLISH_DATE_TAG")
	private String publishDateTag;
	
	@Column(name = "TITLE_TAG")
	private String titleTag;
	
	@Column(name = "BODY_TAG")
	private String bodyTag;
	
	@Column(name = "DATE_FORMAT")
	private String dateFormat;
	
	@Column(name = "REMOVE_KEYWORDS")
	private String removeKeywords;
	
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	
	public String getSearchUrl() {
		return searchUrl;
	}

	public void setSearchUrl(String searchUrl) {
		this.searchUrl = searchUrl;
	}

	public String getListTag() {
		return listTag;
	}

	public void setListTag(String listTag) {
		this.listTag = listTag;
	}

	public String getNewsUrl() {
		return newsUrl;
	}

	public void setNewsUrl(String newsUrl) {
		this.newsUrl = newsUrl;
	}

	public String getPublishDateTag() {
		return publishDateTag;
	}

	public void setPublishDateTag(String publishDateTag) {
		this.publishDateTag = publishDateTag;
	}

	public String getTitleTag() {
		return titleTag;
	}

	public void setTitleTag(String titleTag) {
		this.titleTag = titleTag;
	}

	public String getBodyTag() {
		return bodyTag;
	}

	public void setBodyTag(String bodyTag) {
		this.bodyTag = bodyTag;
	}
	
	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getRemoveKeywords() {
		return removeKeywords;
	}

	public void setRemoveKeywords(String removeKeywords) {
		this.removeKeywords = removeKeywords;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sourceName == null) ? 0 : sourceName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnlistedNewsSource other = (UnlistedNewsSource) obj;
		if (sourceName == null) {
			if (other.sourceName != null)
				return false;
		} else if (!sourceName.equals(other.sourceName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UnlistedNewsSource [sourceId=" + sourceId + ", sourceName=" + sourceName + ", apiType=" + apiType
				+ ", url=" + url + ", searchUrl=" + searchUrl + ", listTag=" + listTag + ", newsUrl=" + newsUrl
				+ ", publishDateTag=" + publishDateTag + ", titleTag=" + titleTag + ", bodyTag=" + bodyTag
				+ ", dateFormat=" + dateFormat + ", removeKeywords=" + removeKeywords + "]";
	}
	

	
}
