package com.finstinct.pipeline.crawler.domain.document;

import org.springframework.data.annotation.Id;

/**
 * Base POJO / bean for all domain classes that represents schema less documents
 * stored in document store
 * 
 * @author arun
 * 
 */
public abstract class BaseDocumentEntity {
	@Id
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
