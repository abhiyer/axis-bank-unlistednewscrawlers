package com.finstinct.pipeline.crawler.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class BaseDAOImpl.
 * 
 * @param <E> the element type
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY, readOnly = false)
public abstract class BaseDAOImpl<E> implements BaseDAO<E> {

    /** The Constant LOG. */
    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    /** The em. */
    @PersistenceContext(name = "entityManagerFactory", unitName = "finstinct")
    protected EntityManager em;

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#findById(java.lang.Object)
     */
    public E findById(Object pk) {

        Class<E> entityClass = findEntityClass();
        return getEntityManager().find(entityClass, pk);
    }
    
    /* (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#findReferenceById(java.lang.Object)
     */
    public E getReferenceById(Object pk) {

        Class<E> entityClass = findEntityClass();
        return getEntityManager().getReference(entityClass, pk);
    }

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#findAll()
     */
    public Set<E> findAll() {

        Class<E> entityClass = findEntityClass();
        TypedQuery<E> q = getEntityManager().createQuery("select x from " + entityClass.getCanonicalName() + " x", entityClass);
        Set<E> retVal = null;
        // if entity implements Comparable, use a sorted set
        if (Comparable.class.isAssignableFrom(entityClass)) {
            retVal = new TreeSet<E>();
        } else {
            retVal = new HashSet<E>();
        }
        retVal.addAll(q.getResultList());
        return retVal;
    }

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#refresh(java.lang.Object)
     */
    public void refresh(Object pk) {

        if (LOG.isDebugEnabled()) {
            Cache cache = getEntityManager().getEntityManagerFactory().getCache();
            Class<E> cls = findEntityClass();
            LOG.debug("refresh object {}[{}]; in cache before: {}",
                new Object[] { cls.getName(), pk.toString(), String.valueOf(cache.contains(cls, pk)) });
        }

        // findById() may get entity from 1st Level Cache, 2nd Level Cache or database
        E entity = findById(pk);
        // refresh() ensures the latest data from database is fetched and 1st Level and 2nd Level caches are updated
        getEntityManager().refresh(entity);

        if (LOG.isDebugEnabled()) {
            Cache cache = getEntityManager().getEntityManagerFactory().getCache();
            Class<E> cls = findEntityClass();
            LOG.debug("refresh object {}[{}]; in cache after: {}",
                new Object[] { cls.getName(), pk.toString(), String.valueOf(cache.contains(cls, pk)) });
        }
    }

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#evict(java.lang.Object)
     */
    public void evict(Object pk) {

        Cache cache = getEntityManager().getEntityManagerFactory().getCache();
        Class<E> cls = findEntityClass();
        if (LOG.isDebugEnabled()) {
        	Object[] strArrBefore = new Object[] { cls.getName(), pk.toString(), String.valueOf(cache.contains(cls, pk)) };
            LOG.debug("evict:before object {}[{}]; in cache before: {}", strArrBefore);
        }
        cache.evict(cls, pk);
        if (LOG.isDebugEnabled()) {
        	Object[] strArrAfter = new Object[] { cls.getName(), pk.toString(), String.valueOf(cache.contains(cls, pk)) };
            LOG.debug("evict:after object {}[{}]; in cache after: {}", strArrAfter);
        }
    }

    /**
     * Find entity class.
     *
     * @return the class
     * @throws ClassCastException the class cast exception
     */
    @SuppressWarnings("unchecked")
    protected Class<E> findEntityClass() throws ClassCastException {

        Class<?> superClass = getClass(); // initial value
        Type superType;

        do {
            superType = superClass.getGenericSuperclass();
            superClass = superType instanceof Class<?> ? (Class<?>) superType : (Class<?>) ((ParameterizedType) superType)
                .getRawType();
        } while (!(superClass.equals(BaseDAOImpl.class)));

        Class<E> entityClass = (Class<E>) ((ParameterizedType) superType).getActualTypeArguments()[0];
        return entityClass;
    }

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#save(java.lang.Object)
     */
    public void save(E entity) {

        getEntityManager().persist(entity);
    }

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#saveCollection(java.util.Collection)
     */
    public void saveCollection(Collection<E> collection) {

        for (E obj : collection) {
            save(obj);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#update(java.lang.Object)
     */
    public E update(E entity) {
        
        E updatedEntity = getEntityManager().merge(entity);
        getEntityManager().flush();
        return updatedEntity;
    }
    
    /* (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#updateCollection(java.util.Collection)
     */
    public Collection<E> updateCollection(Collection<E> collection) {
        
        Collection<E> updatedCollection = null;
        if (collection instanceof List) {
            updatedCollection = new ArrayList<E>();
        } else if (collection instanceof SortedSet) {
            updatedCollection = new TreeSet<E>();
        } else {
            updatedCollection = new HashSet<E>();
        }
        for (E obj : collection) {
            updatedCollection.add(update(obj));
        }
        return updatedCollection;
    }

    /*
     * (non-Javadoc)
     * @see com.extremefin.finstinct.dao.BaseDAO#remove(java.lang.Object)
     */
    public void remove(Object pk) {

        getEntityManager().remove(findById(pk));
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getResultList(String queryStr, Map<String, Object> paramValueMap) {

		Query query = em.createNativeQuery(queryStr);

		Iterator<Entry<String, Object>> iterator = paramValueMap.entrySet().iterator();
		Entry<String, Object> entry;
		while (iterator.hasNext()) {
			entry = iterator.next();
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}

    /**
     * Gets the entity manager.
     *
     * @return the entity manager
     */
    protected EntityManager getEntityManager() {

        return em;
    }
}

