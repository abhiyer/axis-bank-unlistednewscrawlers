package com.finstinct.pipeline.crawler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.finstinct.pipeline.crawler.dao.UnlistedNewsSourcesDAO;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSource;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSourceSchedulerConfig;
import com.finstinct.pipeline.crawler.service.addCompany.GenericNewsLoader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Transactional
public class SchedulerImpl implements Scheduler {
	
	private static final Logger log = LoggerFactory.getLogger(SchedulerImpl.class);
	
	@Autowired
	private UnlistedNewsSourcesDAO unlistedNewsSourcesDao;
	
	@Autowired
	private GenericNewsLoader genericNewsLoader;
	
	//private List<UnlistedNewsSourceSchedulerConfig> newsSourcesConfig;
	
	@Bean
	public List<UnlistedNewsSourceSchedulerConfig> getUnlistedNewsSourceSchedulerConfiguration() {
		
		List<UnlistedNewsSourceSchedulerConfig> newsSourcesConfig = null;
		
		try {
			newsSourcesConfig = new ArrayList<>();
			newsSourcesConfig = unlistedNewsSourcesDao.getUnlistedNewsSourceSchedulerConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newsSourcesConfig;
	}

	//@Scheduled(cron="${dailyTime.cron}")
	public void pullHistoricalNewsDaily() {
		System.out.println("pullHistoricalNewsDaily ran");
		log.debug(" pullHistoricalNewsDaily Scheduling Analyzer...");
		try {
			
		} catch(Exception e ) {
			e.printStackTrace();
		}
	}
	
	//@Scheduled(cron="${historicalTime.cron}")
	public void pullHistoricalNewsForAddedCompanies(int current_day_hour, int previous_day_hour, UnlistedNewsSourceSchedulerConfig unlistedNewsSourceSchedulerConfig) {
		System.out.println("pullHistoricalNewsForAddedCompaniess ran");
		log.debug(" pullHistoricalNewsForAddedCompanies Scheduling Analyzer...");
		
		try {
			Calendar calendar1 = Calendar.getInstance();
			calendar1.add(Calendar.HOUR_OF_DAY, 0);
			Date currentDateTime = calendar1.getTime();
			System.out.println("now :: " + currentDateTime);
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.HOUR_OF_DAY, -48);
			Date previousDateTime = calendar.getTime();
			System.out.println("prev :: " + previousDateTime);
			
			List<UnlistedNewsSourceSchedulerConfig> newsSourcesConfig = new ArrayList<>();
			newsSourcesConfig = getUnlistedNewsSourceSchedulerConfiguration();
			
			for(UnlistedNewsSourceSchedulerConfig newsSourceConfig : newsSourcesConfig)
			{
				//genericNewsLoader.loadGenericNews(newsSourceConfig.getSourceName(), 
				//		newsSourceConfig.getNoOfYrs(), currentDateTime, previousDateTime);
			}
		
			//newsSources = unlistedNewsSourcesDao.getnewsSourceForAddCompany("indianexpress");
		
		
			
			
			//System.out.println(unlistedNewsSourcesDao.getnewsSourceForAddCompany("indianexpress"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	

}
