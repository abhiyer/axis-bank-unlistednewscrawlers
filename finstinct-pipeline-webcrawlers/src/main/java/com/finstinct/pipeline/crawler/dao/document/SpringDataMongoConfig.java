package com.finstinct.pipeline.crawler.dao.document;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@PropertySource("classpath:config/finstinct-docdb-${APP_ENV}.properties")
public class SpringDataMongoConfig extends AbstractMongoConfiguration{
	@Bean
    static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
	
	@Value("${document.database.name}")
	private String databaseName;
	
	@Value("${document.database.host}")
	private String databasehost;

	
	public String getDatabasehost() {
		return databasehost;
	}

	public void setDatabasehost(String databasehost) {
		this.databasehost = databasehost;
	}

	@Bean
	public GridFsTemplate gridFsTemplate() throws Exception {
		return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
	}

	@Override
	protected String getDatabaseName() {
		return databaseName;
	}
	
	public void setDatabaseName(String dbName) {
		this.databaseName = dbName;
	}
	
	@Override
	@Bean
	public Mongo mongo() throws Exception {
		return new MongoClient(databasehost);
	}

}