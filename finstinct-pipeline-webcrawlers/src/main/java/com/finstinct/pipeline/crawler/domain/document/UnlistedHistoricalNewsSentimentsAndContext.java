package com.finstinct.pipeline.crawler.domain.document;

public class UnlistedHistoricalNewsSentimentsAndContext {
	private String text;
	private String label;
	private String score;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "UnlistedHistoricalNewsSentimentsAndContext [text=" + text + ", label=" + label + ", score=" + score
				+ "]";
	}
}
