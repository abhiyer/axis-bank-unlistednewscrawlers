package com.finstinct.pipeline.crawler.domain.document;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="HistoricalNewsItemHolder")
public class HistoricalNewsItemHolder extends BaseDocumentEntity{

private Integer sourceId;
private HistoricalNewsItemDoc historicalNewsItem;
private List<Integer> stockIds;        
private UnlistedHistoricalNewsSentimentsAndContext sentiment;
private UnlistedHistoricalNewsSentimentsAndContext context;

private String newsSummary;

public Integer getSourceId() {
	return sourceId;
}

public void setSourceId(Integer sourceId) {
	this.sourceId = sourceId;
}

public HistoricalNewsItemDoc getHistoricalNewsItem() {
	return historicalNewsItem;
}

public void setHistoricalNewsItem(HistoricalNewsItemDoc historicalNewsItem) {
	this.historicalNewsItem = historicalNewsItem;
}

public List<Integer> getStockIds() {
	return stockIds;
}

public void setStockIds(List<Integer> stockIds) {
	this.stockIds = stockIds;
}

public String getNewsSummary() {
	return newsSummary;
}

public void setNewsSummary(String newsSummary) {
	this.newsSummary = newsSummary;
}

public UnlistedHistoricalNewsSentimentsAndContext getSentiment() {
	return sentiment;
}

public void setSentiment(UnlistedHistoricalNewsSentimentsAndContext sentiment) {
	this.sentiment = sentiment;
}

public UnlistedHistoricalNewsSentimentsAndContext getContext() {
	return context;
}

public void setContext(UnlistedHistoricalNewsSentimentsAndContext context) {
	this.context = context;
}

@Override
public String toString() {
	return "HistoricalNewsItemHolder [sourceId=" + sourceId + ", historicalNewsItem=" + historicalNewsItem
			+ ", stockIds=" + stockIds + ", sentiment=" + sentiment + ", context=" + context + ", newsSummary="
			+ newsSummary + "]";
}


	
}
