package com.finstinct.pipeline.crawler.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "entity_news_item")
public class EntityNewsItem {

	@Id
	@GeneratedValue
	@Column(name = "ITEM_ID")
	private Integer itemId;

	@Column(name = "ENTITY_ID")
	private Integer entityId;
	
//	@OneToOne(fetch = FetchType.LAZY,mappedBy="entityNewsItem",cascade=CascadeType.ALL)
//	private CustomizedSummaryReport customizedSummaryReport;
	// @ManyToOne(targetEntity=Enterprise.class)
	// @JoinColumn(name = "ENTERPRISE_ID")
	// private Enterprise enterprise = null;

	@Column(name = "ENTERPRISE_ID")
	private Integer enterpriseId;

	@Column(name = "ITEM_DATE")
	private Date itemDate;

	@Column(name = "LINK_HASH")
	private String linkHash;

	@Column(name = "SOURCE_ID")
	private Integer sourceId;

	@Column(name = "PUBLICATION_ID")
	private Integer publicationId;

	@Column(name = "TRIGGER_DOC_REF")
	private String triggerDocRef;

	@Column(name = "NEWS_DOC_REF")
	private String newsDocRef;

	@Column(name = "UPDATE_TIME")
	private Date updateTime;

	@Column(name = "TOTAL_POSITIVE")
	private Integer totalPositive;

	@Column(name = "TOTAL_NEGATIVE")
	private Integer totalNegative;

	@Column(name = "TOTAL_NEUTRAL")
	private Integer totalNeutral;

	@Column(name = "URL")
	private String url;

	@Column(name = "DUPLICATE_REFRENCE_ID")
	private String duplicateReferenceId;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "IMAGE_URL")
	private String imageUrl;
	
	@OneToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="ENTITY_NEWS_ITEM_ID")
	private Set <CustomizedSummaryReport> customizedSummaryReport;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLinkHash() {
		return linkHash;
	}

	public void setLinkHash(String linkHash) {
		this.linkHash = linkHash;
	}

	public Integer getTotalPositive() {
		return totalPositive;
	}

	public void setTotalPositive(Integer totalPositive) {
		this.totalPositive = totalPositive;
	}

	public Integer getTotalNegative() {
		return totalNegative;
	}

	public void setTotalNegative(Integer totalNegative) {
		this.totalNegative = totalNegative;
	}

	public Integer getTotalNeutral() {
		return totalNeutral;
	}

	public void setTotalNeutral(Integer totalNeutral) {
		this.totalNeutral = totalNeutral;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public Date getItemDate() {
		return itemDate;
	}

	public void setItemDate(Date itemDate) {
		this.itemDate = itemDate;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getPublicationId() {
		return publicationId;
	}

	public void setPublicationId(Integer publicationId) {
		this.publicationId = publicationId;
	}

	public String getTriggerDocRef() {
		return triggerDocRef;
	}

	public void setTriggerDocRef(String triggerDocRef) {
		this.triggerDocRef = triggerDocRef;
	}

	public String getNewsDocRef() {
		return newsDocRef;
	}

	public void setNewsDocRef(String newsDocRef) {
		this.newsDocRef = newsDocRef;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getDuplicateReferenceId() {
		return duplicateReferenceId;
	}

	public void setDuplicateReferenceId(String duplicateReferenceId) {
		this.duplicateReferenceId = duplicateReferenceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "EntityNewsItem [itemId=" + itemId + ", entityId=" + entityId + ", enterpriseId=" + enterpriseId
				+ ", itemDate=" + itemDate + ", linkHash=" + linkHash + ", sourceId=" + sourceId + ", publicationId="
				+ publicationId + ", triggerDocRef=" + triggerDocRef + ", newsDocRef=" + newsDocRef + ", updateTime="
				+ updateTime + ", totalPositive=" + totalPositive + ", totalNegative=" + totalNegative
				+ ", totalNeutral=" + totalNeutral + ", url=" + url + ", duplicateReferenceId=" + duplicateReferenceId
				+ ", status=" + status + ", imageUrl=" + imageUrl + "]";
	}

}
