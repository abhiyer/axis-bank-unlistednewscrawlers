package com.finstinct.pipeline.crawler.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finstinct.pipeline.crawler.domain.EntityNewsItem;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsCompanies;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSource;

@Repository
@Transactional
public class UnlistedNewsCompaniesDAOImpl extends BaseDAOImpl<UnlistedNewsCompanies>
		implements UnlistedNewsCompaniesDAO {

	public void saveUnlistedNewsCompanies(UnlistedNewsCompanies unlistedNewsCompanies) {

		if (unlistedNewsCompanies.getCompanyId() == null) {
			save(unlistedNewsCompanies);
		} else {
			update(unlistedNewsCompanies);
		}
	}

	@Override
	public List<UnlistedNewsCompanies> getUnlistedNewsCompanies(Integer enterpriseId) {
		return getEntityManager().createQuery("SELECT en from UnlistedNewsCompanies en where en.enterpriseId=:enterpriseId  ORDER BY en.companyId desc",
				UnlistedNewsCompanies.class).setParameter("enterpriseId", enterpriseId).getResultList();
	}

	@Override
	public String getAlreadyExistsCompanyName(String companyName) {
		String name = "";

		List<UnlistedNewsCompanies> listOfRecords = getEntityManager()
				.createQuery("SELECT en from UnlistedNewsCompanies en WHERE en.companyName=:companyName ",
						UnlistedNewsCompanies.class)
				.setParameter("companyName", companyName).getResultList();
		if (listOfRecords != null && !listOfRecords.isEmpty()) {
			name = listOfRecords.get(0).getCompanyName();
		}
		return name;
	}

	@Override
	public String findExistingCompanyName(String companyName) {
		String name = "";
		List<UnlistedNewsCompanies> listOfRecords = getEntityManager()
				.createQuery("SELECT en from UnlistedNewsCompanies en WHERE en.companyName=:companyName)",
						UnlistedNewsCompanies.class)
				.setMaxResults(1).setParameter("companyName", companyName).getResultList();

		if (listOfRecords != null && !listOfRecords.isEmpty()) {
			name = listOfRecords.get(0).getCompanyName();
		}
		return name;
	}

	@Override
	public List<UnlistedNewsCompanies> findNewCompanies(Date yesterdaydate, Date todayDate) {

		return getEntityManager()
				.createQuery(
						"SELECT en from UnlistedNewsCompanies en WHERE en.dateCreated>=:yesterdaydate AND en.dateCreated<=:todayDate)",
						UnlistedNewsCompanies.class)
				.setParameter("yesterdaydate", yesterdaydate).setParameter("todayDate", todayDate).getResultList();
	}

	// @Override
	// public List<UnlistedNewsCompanies> findCompanyEmailsData(Integer
	// companyId) {
	//
	// return getEntityManager().createQuery(
	// "SELECT en from UnlistedNewsCompanies en WHERE en.companyId=:companyId)",
	// UnlistedNewsCompanies.class).
	// setParameter("companyId",companyId).
	// setMaxResults(1).getResultList();
	// }

	@Override
	public UnlistedNewsCompanies findCompanyEmailsData(Integer companyId) {

		return getEntityManager().find(UnlistedNewsCompanies.class, companyId);

	}

	@Override
	public List<UnlistedNewsCompanies> findCompanyrecordByCompanyName(String companyName) {

		return getEntityManager()
				.createQuery("SELECT en from UnlistedNewsCompanies en WHERE en.companyName =:companyName",
						UnlistedNewsCompanies.class)
				.setParameter("companyName", companyName).getResultList();
	}
	
	
	@Override
	public List<UnlistedNewsCompanies> findAlreadySavedCompany(String companyName) {
		List<UnlistedNewsCompanies> listOfRecords = getEntityManager().createQuery(
				"SELECT en from UnlistedNewsCompanies en WHERE en.companyName=:companyName)",
				UnlistedNewsCompanies.class).
				setMaxResults(1).
				setParameter("companyName",companyName).getResultList();
		
		return listOfRecords;
	}
	
	@Override
	public List<UnlistedNewsCompanies> getCompanyName(String companyName) {
		return getEntityManager()
				.createQuery(
						"SELECT en from UnlistedNewsCompanies en where en.companyName like :companyName",
						UnlistedNewsCompanies.class)
				.setParameter("companyName", "%"+companyName+"%").getResultList();
	}

	@Override
	public List<UnlistedNewsCompanies> findUnlistedCompaniesByEnterpriseId(Integer enterpriseId) {
		
		return getEntityManager().createQuery(
				"SELECT en from UnlistedNewsCompanies en where en.enterpriseId = :enterpriseId order by companyId asc",
				UnlistedNewsCompanies.class).setParameter("enterpriseId", enterpriseId).getResultList();
	}


}
