package com.finstinct.pipeline.crawler.service.addCompany;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.transaction.Transactional;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.finstinct.pipeline.crawler.domain.HistoricalNewsCrawlerLog;
import com.finstinct.pipeline.crawler.domain.HistoricalNewsItem;
import com.finstinct.pipeline.crawler.dao.HistoricalNewsCrawlerLogDAO;
import com.finstinct.pipeline.crawler.dao.HistoricalNewsItemDAO;
import com.finstinct.pipeline.crawler.dao.UnlistedNewsCompaniesDAO;
import com.finstinct.pipeline.crawler.dao.UnlistedNewsSourcesDAO;
import com.finstinct.pipeline.crawler.dao.document.DocumentStore;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsCompanies;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSource;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSourceSchedulerConfig;
import com.finstinct.pipeline.crawler.domain.document.HistoricalNewsItemDoc;
import com.finstinct.pipeline.crawler.domain.document.HistoricalNewsItemHolder;
import com.finstinct.pipeline.crawler.domain.document.UnlistedHistoricalNewsSentimentsAndContext;
import com.finstinct.pipeline.crawler.historicalnews.HistoricalNewsFeedItems;
import com.finstinct.pipeline.crawler.service.DateUtil;
import com.vdurmont.emoji.EmojiParser;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Component
@Transactional
public class GenericNewsLoader {
	
	final Logger LOG = LoggerFactory.getLogger(GenericNewsLoader.class);
	
	
	@Autowired
	private UnlistedNewsSourcesDAO unlistedNewsSourcesDao;
	
	@Autowired
	private UnlistedNewsCompaniesDAO unlistedNewsCompaniesDao;
	
	@Autowired
	private HistoricalNewsCrawlerLogDAO historicalNewsCrawlerLogDao;
	
	@Autowired
	private GenericNewsSourceCrawlerAddCompany genericNewsSourceCrawlerAddCompany;
	
	@Autowired
	private HistoricalNewsItemPersisterAddCompany historicalNewsItemPersister;
	
	@Autowired
	private HistoricalNewsItemDAO historicalNewsItemDao;
	
	@Autowired
	private DocumentStore docStore;
	
	@Value("#{props[newsSentimentServiceUrl]}")
	private String newsSentimentServiceUrl;
	
	@Value("#{props[newsContextServiceUrl]}")
	private String newsContextServiceUrl;
	
	@Value("#{props[duplicateNewsExtractionServiceUrl]}")
	private String duplicateNewsExtractionServiceUrl;
	
	
	
	public void loadGenericNews(UnlistedNewsSourceSchedulerConfig unlistedNewsSourceSchedulerConfig, Date current, Date previous) {
		
		
		List<UnlistedNewsSource> newsSources = new ArrayList<>();
		
		try {
			//--- to be a combined call with news source crawler config ---//
			newsSources = unlistedNewsSourcesDao.getnewsSourceForAddCompany(unlistedNewsSourceSchedulerConfig.getSourceName());
		} catch (Exception e) {
			LOG.error(""+unlistedNewsSourceSchedulerConfig.getSourceName()+"Loader_ERROR 1:" +e.getMessage(),e);
			e.printStackTrace();
		}
		
		try {
			
			List<UnlistedNewsCompanies> companies = unlistedNewsCompaniesDao.findNewCompanies(previous,
					current);
			
			for (UnlistedNewsSource newsSource : newsSources) {
				
				for (UnlistedNewsCompanies unlistedNewsCompanies : companies) {
				/////// save historicalNewsCrawlerLog StartDatetime for this Crawler
				HistoricalNewsCrawlerLog historicalNewsCrawlerLog = new HistoricalNewsCrawlerLog();
				historicalNewsCrawlerLog.setSourceId(newsSource.getSourceId());
				historicalNewsCrawlerLog.setCompanyId(unlistedNewsCompanies.getCompanyId());
				historicalNewsCrawlerLog.setStartDate(new Date());
				historicalNewsCrawlerLogDao.addUpdateHistoricalNewsCrawlerLog(historicalNewsCrawlerLog);
				////// saving ends here ....
				
				List<HistoricalNewsFeedItems> newsItems = genericNewsSourceCrawlerAddCompany
						.getGenericNews(unlistedNewsCompanies, newsSource, unlistedNewsSourceSchedulerConfig);
				//System.out.println(" newsItems :::: " + newsItems);
				if (newsItems == null) {
					continue;
				} else {
					processNewsForUnlistedCompany(newsSource, newsItems, unlistedNewsCompanies);
				}
					
				}
				
			}
			
			
		} catch(Exception e) {
			LOG.error(""+unlistedNewsSourceSchedulerConfig.getSourceName()+"Loader_ERROR 2:" +e.getMessage(),e);
			e.printStackTrace();
		}
		
	}
	
	public void processNewsForUnlistedCompany(UnlistedNewsSource newsSource, List<HistoricalNewsFeedItems> newsItems,
			UnlistedNewsCompanies unlistedNewsCompanies) throws UnsupportedEncodingException {
	
	
		for (HistoricalNewsFeedItems newsItem : newsItems) {
		
			try {
				if (newsItem.isItemAlreadyFetched()) {
				
					continue;
				}
				
				if (newsItem.getStocks().size() == 1) {
					newsItem.setSourceId(newsSource.getSourceId());
					newsItem.setSourceName(newsSource.getSourceName());

					saveNewsForUnlistedCompany(newsItem, newsItem.getStocks(),unlistedNewsCompanies);
				} else if (newsItem.getStocks().size() > 1) {
					Map<Integer, String> stocksNew = new HashMap<Integer, String>();
					int count = 0;
					for (Entry<Integer, String> entry : newsItem.getStocks().entrySet()) {
						
						if (count == 0) {
							stocksNew.put(entry.getKey(), entry.getValue());
						}
						count++;
					}
					
					newsItem.setSourceId(newsSource.getSourceId());
					newsItem.setSourceName(newsSource.getSourceName());
					saveNewsForUnlistedCompany(newsItem, stocksNew,unlistedNewsCompanies);
				} else {
		
				}

			

			} catch (Exception e) {
				LOG.error("ERROR_processNewsForUnlistedCompany : " + e.getMessage(), e);
			}

		}
	
	}
	
	private void saveNewsForUnlistedCompany(HistoricalNewsFeedItems newsItems, Map<Integer, String> stocks, UnlistedNewsCompanies unlistedNewsCompanies) {

		Integer[] entityIds = new Integer[1];
		newsItems.setCompanyId(stocks.keySet().toArray(entityIds)[0]);
		String compName = stocks.get(entityIds[0]);
		newsItems.setCompanyName(unlistedNewsCompanies.getCompanyName());
		
		UnlistedHistoricalNewsSentimentsAndContext sentiment = newsItemAnalysisAPI(newsItems, newsSentimentServiceUrl);
		UnlistedHistoricalNewsSentimentsAndContext context = newsItemAnalysisAPI(newsItems, newsContextServiceUrl);
	    List<String> dataList = storeEventTriggersToMongo(newsItems, stocks, sentiment, context);
		 
		 String unlistedNewsItemDocId = dataList.get(1);
		 String newsDataObject = EmojiParser.parseToAliases(dataList.get(0));
		 
		// System.out.println("newsDataObject :: " + newsDataObject);
		//System.out.println("unlistedNewsItemDocId ::: " + unlistedNewsItemDocId);
			// storeNewsItemToDB(newsItems,stocks,unlistedNewsItemDocId);
			historicalNewsItemPersister.storeNewsItemToDB(newsItems, stocks, 
							unlistedNewsItemDocId, newsDataObject);
			try {
				checkingDuplicateHistoricalNewsNews(newsItems, stocks);
			} catch (Exception exception) {
				LOG.error("Inside Dup Exception Pyhthon Input For At Starting" + exception.getMessage(),exception);
			}
		

	}
	
	private UnlistedHistoricalNewsSentimentsAndContext newsItemAnalysisAPI(HistoricalNewsFeedItems newsItem, String analyserServiceUrl) {
		
		UnlistedHistoricalNewsSentimentsAndContext unlistedHistoricalNewsSentimentsAndContext = new UnlistedHistoricalNewsSentimentsAndContext();
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			
			ResponseEntity<String> response = restTemplate.getForEntity(analyserServiceUrl + "?text=" + newsItem.getTitle(),
					String.class);
			//System.out.println("Python Output For Historical News analysis " + response.getBody().toString());
			//LOG.debug("Python Output For Historical News analysis " + response.getBody().toString());
			
			JSONObject itemToCheck = new JSONObject();
			itemToCheck.put("callBack", response.getBody().toString());
			JSONObject analysisObject = itemToCheck.getJSONObject("callBack");
			
			unlistedHistoricalNewsSentimentsAndContext.setLabel(analysisObject.getString("label"));
			unlistedHistoricalNewsSentimentsAndContext.setText(analysisObject.getString("text"));
			unlistedHistoricalNewsSentimentsAndContext.setScore(analysisObject.getString("score"));
			
		} catch(Exception e) {
			LOG.error("analyseNewsItemSentimentAndContext 1: " + e.getMessage(), e);
		}
		
		return unlistedHistoricalNewsSentimentsAndContext;
	}
	
	private List<String> storeEventTriggersToMongo(HistoricalNewsFeedItems newsItem, Map<Integer, String> stocks, 
			UnlistedHistoricalNewsSentimentsAndContext sentiment, UnlistedHistoricalNewsSentimentsAndContext context) {

		HistoricalNewsItemDoc historicalItemDoc = new HistoricalNewsItemDoc();
		HistoricalNewsItemHolder newsItemHolder = new HistoricalNewsItemHolder();
		ObjectMapper newsObjectToStringMapper = new ObjectMapper();
		
		List<String> dataList = new ArrayList<>();
		
		try {
		
		historicalItemDoc.setAuthor(newsItem.getAuthor());
		historicalItemDoc.setDescription(newsItem.getDescription());
		historicalItemDoc.setPublishedDate(newsItem.getPublishedDate());
		historicalItemDoc.setTitle(newsItem.getTitle());
		historicalItemDoc.setUrl(newsItem.getUrl());
		historicalItemDoc.setSourceName(newsItem.getSourceName());
		historicalItemDoc.setNews(newsItem.getNews());
		historicalItemDoc.setLink(newsItem.getUrl());
		historicalItemDoc.setImageUrl(newsItem.getImageUrl());
		
		newsItemHolder.setSourceId(newsItem.getSourceId());
		Set<Integer> stockIds = stocks.keySet();
		newsItemHolder.setStockIds(Arrays.asList(stockIds.toArray(new Integer[stockIds.size()])));
		newsItemHolder.setHistoricalNewsItem(historicalItemDoc);
		newsItemHolder.setContext(context);
		newsItemHolder.setSentiment(sentiment);
		newsItemHolder.setNewsSummary(newsItem.getNewsSummary());
		
		
		dataList.add(newsObjectToStringMapper.writeValueAsString(newsItemHolder));
		dataList.add(docStore.storeDocument(newsItemHolder.getClass().getName().toLowerCase(), newsItemHolder));
		
		
		} catch (Exception exception) {
			LOG.error("ERROR_storeEventTriggersToMongo 1: " + exception.getMessage(), exception);
		}

		
		return dataList;
		//return docStore.storeDocument(newsItemHolder.getClass().getName().toLowerCase(), newsItemHolder);
	}
	
	
	private void checkingDuplicateHistoricalNewsNews(HistoricalNewsFeedItems newsItems, Map<Integer, String> entities) {

		try {

			LOG.debug("Inside Dup  Pyhthon Input For");
			Integer[] entityIds = new Integer[1];
			Date startDate = DateUtil.getTodaysDate();
			List<HistoricalNewsItem> historicalNewsItems = historicalNewsItemDao
					.getHistoricalNews(entities.keySet().toArray(entityIds)[0], startDate);
			if (!historicalNewsItems.isEmpty()) {

				JSONArray newsitems = new JSONArray();
				for (HistoricalNewsItem historicalNewsItem : historicalNewsItems) {
					try {
						JSONObject jsonObject = new JSONObject();
						UnlistedNewsCompanies entity = unlistedNewsCompaniesDao
								.findById(historicalNewsItem.getEntityId());

						HistoricalNewsItemHolder newsDocRef = docStore.readDocument(
								HistoricalNewsItemHolder.class.getName().toLowerCase(),
								historicalNewsItem.getTriggeredDocRef(), HistoricalNewsItemHolder.class);

						HistoricalNewsItemDoc item = new HistoricalNewsItemDoc();
						item = newsDocRef.getHistoricalNewsItem();
						jsonObject.put("ItemId", historicalNewsItem.getItemId());
						jsonObject.put("EntityCode", entity.getCode());
						jsonObject.put("Title", item.getTitle());
						jsonObject.put("Published_date", startDate.toString());
						newsitems.add(jsonObject);
					} catch (Exception exception) {
						LOG.error("Inside Dup Exception Pyhthon Input For" + exception.getMessage(),exception);
						return;
					}
				}
				LOG.debug("Pyhthon Input For JSON ARRAY Dup News " + newsitems);
				// JSONObject itemToCheck = new JSONObject();
				// itemToCheck.put("newsitems", newsitems);
				RestTemplate restTemplate = new RestTemplate();
				ObjectMapper mapper = new ObjectMapper();
				Map<String, JSONArray> requestMap = new HashMap<String, JSONArray>();
				requestMap.put("newsitems", newsitems);
				String requestData;
				String text = new String();
				try {
					requestData = mapper.writeValueAsString(requestMap);
					LOG.debug("Pyhthon Input For duplicate News " + requestData.toString());
					ResponseEntity<String> response = restTemplate
							.postForEntity(new URI(duplicateNewsExtractionServiceUrl), requestData, String.class);
					text = response.getBody();
					LOG.debug("Pyhthon OutPut For duplicate News " + text.toString());
				} catch (Exception exception) {
					
					LOG.error("Pyhthon OutPut For duplicate News EXception " + exception.getMessage(),exception);
					return;
				}
				JSONObject itemToCheck = new JSONObject();
				itemToCheck.put("callBack", text);
				LOG.debug("Inside Dup  Pyhthon OutPut JSONArray CallBack For" + itemToCheck);
				JSONArray newsitems1 = itemToCheck.getJSONArray("callBack");
				LOG.debug("Inside Dup  Pyhthon OutPut JSONArray For" + newsitems1);
				for (int i = 0; i < newsitems1.size(); i++) {
					JSONObject jsonobject = newsitems1.getJSONObject(i);
					LOG.debug("Inside Dup  Pyhthon OutPut JSONArrayOBJECT For" + jsonobject);
					Integer ItemId = jsonobject.getInt("ItemId");
					LOG.debug("ItemId of newsId  for news: " + ItemId);
					String Title = jsonobject.getString("Title");
					if (jsonobject.getString("Duplicate_Key_y") == null) {
						continue;
					}
					String Duplicate_Key = jsonobject.getString("Duplicate_Key_y");
					if (Duplicate_Key != null && !Duplicate_Key.isEmpty()) {
						HistoricalNewsItem HistoricalNewsItemDup = historicalNewsItemDao.findById(ItemId);
						HistoricalNewsItemDup.setDuplicateReferenceId(Duplicate_Key);
						historicalNewsItemDao.addUpdateHistoricalNewsItem(HistoricalNewsItemDup);
					}
				}
			}

		} catch (Exception exception) {
			LOG.error("ERROR_IndianExpressNewsLoader :" +exception.getMessage(),exception);
			return;
		}

	}
	
	
	
	
	

}
