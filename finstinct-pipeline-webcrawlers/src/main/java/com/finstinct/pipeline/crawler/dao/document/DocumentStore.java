package com.finstinct.pipeline.crawler.dao.document;

import java.util.List;

import com.finstinct.pipeline.crawler.domain.document.BaseDocumentEntity;
import com.finstinct.pipeline.crawler.domain.document.HistoricalNewsItemDoc;


/**
 * Document file store into MongoDB
 * 
 */
public interface DocumentStore {
	/**
	 * Stores given Java object into the document store
	 * 
	 * @param collectionName
	 * @param entity
	 *            java object to be stored in the given collection
	 * @return object id of the inserted document
	 */
	String storeDocument(String collectionName, BaseDocumentEntity entity);

	/**
	 * Retrieves document based on given collection, document id and constructs
	 * an object of given entityClass type
	 * 
	 * @param collectionName
	 * @param id
	 * @param entityClass
	 * @return
	 */
	<T> T readDocument(String collectionName, String id, Class<T> entityClass);

	/**
	 * Retrieves all documents in the given collection
	 * 
	 * @param entityClass
	 *            Type of object to be constructed from the documents retrieved
	 * @param collectionName
	 *            Name of the collection under which required docs are stored
	 * @return
	 */
	<T> List<T> getDocuments(Class<T> entityClass, String collectionName);

	/**
	 * Queries document with given filter order pair where each filter is a pair
	 * of key followed by value. At present only
	 * 
	 * @param entityClass
	 * @param collectionName
	 * @param filters
	 * @return
	 */
	<T> List<T> queryDocuments(Class<T> entityClass, String collectionName, Object... filters);

	/**
	 * Query document store for all documents having given ids
	 * 
	 * @param class1
	 * @param lowerCase
	 * @param objectIds
	 * @return
	 */
	<T> List<T> queryDocumentsOnId(Class<T> class1, String lowerCase, String objectId);
	
	<T> List<T> queryDocumentsBasedOnIds(Class<T> class1, String lowerCase, List<String> objectIds);
	/**
	 * @param collectionName
	 * @param docRefId
	 */
	void updateDocumentBasedOnId(String balaceSheetTable,String docRefId,BaseDocumentEntity entity);
	
	
	
	/**
	 * @param collectionName
	 * @param docRefId
	 */
	void updateSummaryReportDocumentBasedOnId(String editedNewsSummary,String docRefId,BaseDocumentEntity entity);
	
	
	/**
	 * @param collectionName
	 * @param docRefId
	 */
	void updateUnlistedCustomizedSummaryReportDocumentBasedOnId(HistoricalNewsItemDoc newsItem,String customizedNewsSummary,String docRefId,BaseDocumentEntity entity);

	/**
	 * @param collectionName
	 * @param docRefId
	 */
	void updateUnlistedSummaryReportDocumentBasedOnId(String editedNewsSummary,String docRefId,BaseDocumentEntity entity);
	
	
	
	
	
	<T> List<T> queryDocumentsBasedOnDocRefIds(Class<T> class1, String lowerCase, String documentRefId);

	void updateTableCsvInMongo(String tableCsv, String mongoRefId, BaseDocumentEntity entity);

	void deleteDocTypeTrainingDocumentById(String documentRefId, BaseDocumentEntity entityClass);

}
