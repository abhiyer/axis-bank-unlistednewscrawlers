package com.finstinct.pipeline.crawler.dao;

import java.util.Date;
import java.util.List;

import com.finstinct.pipeline.crawler.domain.EnterpriseCompanyEmailMapping;
import com.finstinct.pipeline.crawler.domain.EnterpriseUserSentimentContextMapping;
import com.finstinct.pipeline.crawler.domain.HistoricalNewsItem;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSource;

public interface HistoricalNewsItemDAO extends BaseDAO<HistoricalNewsItem> {

	Date findLastUpdatedDate(int sourceId);

	void addUpdateHistoricalNewsItem(HistoricalNewsItem historicalNewsItem);

	boolean itemExists(String url, Date publishedDate);

	List<HistoricalNewsItem> getHistoricalNews(int entityId, Date startDate);

	List<Object[]> getUnlistedCustomizedSummaryReportWithCriteria(List<Integer> companyIds, String status,
			Date dateFrom, Date dateTo, Integer enterpriseId, Date todaysDate);

	List<EnterpriseCompanyEmailMapping> getEmailsForComapny(Integer companyId, String ComapnyName);

	boolean sentimentContextMappingExists(int enterpriseId, String sentiment, String context);

	List<EnterpriseCompanyEmailMapping> getEmailsForComapnyForEnterprise(int enterpriseId);

	List<EnterpriseUserSentimentContextMapping> getsentimentContextForEnterprise(int enterpriseId);

	String getMaxDateForCompanyRecord(UnlistedNewsSource newsSource);

	String getMinDateForComapnyRecord(UnlistedNewsSource newsSource);

	List<HistoricalNewsItem> getHistoricalNews();

	List<HistoricalNewsItem> findNewsRecordsByEntityId(Integer entityId);

	List<HistoricalNewsItem> findNewlyStoredNewsItems(Integer sourceId);
	
	List<HistoricalNewsItem> findHistoricalNewsRecordsBasedOnEntityAndSource(Integer sourceId, Integer entityId);

	List<HistoricalNewsItem> getNewsFetchedToday();
	
	List<HistoricalNewsItem> getNewsFetchedTodayForEntity(Integer entityId);
	
	List<HistoricalNewsItem> findPendingHandoffNews();

	List<HistoricalNewsItem> getNewsForSourceAndNewCompanyHasNotSentEmail(Integer sourceId, Integer companyId, Date currentDateTime, Date previousDateTime);
}
