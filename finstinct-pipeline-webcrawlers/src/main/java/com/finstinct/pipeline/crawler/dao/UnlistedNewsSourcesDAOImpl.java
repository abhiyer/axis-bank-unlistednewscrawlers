package com.finstinct.pipeline.crawler.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

//import com.extremefin.finstinct.domain.EntityNewsSource;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsCompanies;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSource;
import com.finstinct.pipeline.crawler.domain.UnlistedNewsSourceSchedulerConfig;

@Repository
@Transactional
public class UnlistedNewsSourcesDAOImpl extends BaseDAOImpl<UnlistedNewsSource> implements UnlistedNewsSourcesDAO{

	
	public void addUpdateNewsSource(UnlistedNewsSource source) {
		if(source.getSourceId() == null){
			save(source);
		}else{
			update(source);
		}
		
	}
	
	
	public List<UnlistedNewsSource> getAllUnlistedNewsSources() {
		// TODO Auto-generated method stub
		return getEntityManager()
				.createQuery("SELECT en from UnlistedNewsSource en " + "ORDER BY en.sourceId asc", UnlistedNewsSource.class)
				.getResultList();
		
	}

	public String findExistingCompanyName(String companyName, String synonyms) {
		String name= "";
		List<UnlistedNewsCompanies> listOfRecords = getEntityManager().createQuery(
				"SELECT en from UnlistedNewsCompanies en WHERE en.companyName=:companyName)",
				UnlistedNewsCompanies.class).
				setMaxResults(1).
				setParameter("companyName",companyName).getResultList();
		
		if (listOfRecords != null && !listOfRecords.isEmpty()) {
			name = listOfRecords.get(0).getCompanyName();
		}
		return name;
	}
	
	public List<UnlistedNewsSource> getnewsSourceForAddCompany(String sourceName) {
		// TODO Auto-generated method stub
		return getEntityManager().createQuery(
				"SELECT en from UnlistedNewsSource en WHERE en.sourceName=:sourceName)",
				UnlistedNewsSource.class).
				setMaxResults(1).
				setParameter("sourceName",sourceName).getResultList();
	}

	public String findExistingSourceName(String sourceName) {
	
		String name = "";
		List<UnlistedNewsSource> listOfRecords = getEntityManager()
				.createQuery("SELECT en from UnlistedNewsSource en WHERE en.sourceName=:sourceName)",
						UnlistedNewsSource.class)
				.setMaxResults(1).setParameter("sourceName", sourceName).getResultList();

		if (listOfRecords != null && !listOfRecords.isEmpty()) {
			name = listOfRecords.get(0).getSourceName();
		}
		return name;

	}


	public List<UnlistedNewsSource> findSourceBySourceId(Integer sourceId) {
		
		return getEntityManager().createQuery(
				"SELECT en from UnlistedNewsSource en WHERE en.sourceId=:sourceId)",
				UnlistedNewsSource.class).
				setMaxResults(1).
				setParameter("sourceId",sourceId).getResultList();
	}


	@Override
	public List<UnlistedNewsSourceSchedulerConfig> getUnlistedNewsSourceSchedulerConfiguration() {
		// TODO Auto-generated method stub
		return getEntityManager()
				.createQuery("SELECT en from UnlistedNewsSourceSchedulerConfig en " + "ORDER BY en.sourceId asc", UnlistedNewsSourceSchedulerConfig.class)
				.getResultList();
	}

	

}
