package com.finstinct.pipeline.crawler.dao.document;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.finstinct.pipeline.crawler.domain.document.BaseDocumentEntity;
import com.finstinct.pipeline.crawler.domain.document.HistoricalNewsItemDoc;


/**
 * GridFs based file store into MongoDB
 * 
 */
@Component
public class DocumentStoreImpl implements DocumentStore {
	private static final Logger log = LoggerFactory.getLogger(DocumentStoreImpl.class);

	@Autowired
	private MongoOperations mongoTemplate;

	public MongoOperations getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoOperations mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public String storeDocument(String collectionName, BaseDocumentEntity entity) {

		try {
			mongoTemplate.insert(entity, collectionName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String id = null;
		try {
			id = (String) entity.getClass().getMethod("getId").invoke(entity);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			log.error("Error while storing document in doc store collection " + collectionName, e);
		}
		return id;
	}

	@Override
	public <T> T readDocument(String collectionName, String id, Class<T> entityClass) {
		return mongoTemplate.findById(id, entityClass, collectionName);
	}

	@Override
	public <T> List<T> getDocuments(Class<T> entityClass, String collectionName) {
		return mongoTemplate.findAll(entityClass, collectionName);
	}

	@Override
	public <T> List<T> queryDocuments(Class<T> entityClass, String collectionName, Object... filters) {
		Criteria criteria = new Criteria();
		if (filters != null) {
			for (int i = 0; i < (filters.length / 2); i++) {
				String key = filters[i * 2].toString();
				Object value = filters[(i * 2) + 1];

				criteria = criteria.andOperator(Criteria.where(key).is(value));
			}
		}
		return mongoTemplate.find(new Query(criteria), entityClass, collectionName);
	}
	
	@Override
	public <T> List<T> queryDocumentsOnId(Class<T> entityClass, String collectionName, String objectId) {
		if (objectId != null) {
			String key = "_id";

			Criteria criteria = Criteria.where(key).is(new ObjectId(objectId));

			log.info(criteria.toString());
			
			return mongoTemplate.find(new Query(criteria), entityClass, collectionName);
		}
		return null;
	}

	@Override
	public <T> List<T> queryDocumentsBasedOnIds(Class<T> entityClass, String collectionName, List<String> objectIds) {
		if ((objectIds != null) && (objectIds.size() > 0)) {
			String key = "_id";

			Criteria criteria = Criteria.where(key).in(objectIds);

			log.info(criteria.toString());
			
			return mongoTemplate.find(new Query(criteria), entityClass, collectionName);
		}
		return null;
	}

	@Override
	public void updateDocumentBasedOnId(String balaceSheetTable, String docRefId, BaseDocumentEntity entityClass) {
		if ((docRefId != null) && (!docRefId.isEmpty())) {
			String key = "_id";

			Criteria criteria = Criteria.where(key).in(docRefId);
			Update update = new Update();
			update.set("balanceSheetTable", balaceSheetTable);
			mongoTemplate.updateFirst(new Query(criteria), update, entityClass.getClass().getName().toLowerCase());
		}

	}


	@Override
	public void updateSummaryReportDocumentBasedOnId(String editedNewsSummary, String docRefId,
			BaseDocumentEntity entityClass) {
		// TODO Auto-generated method stub
				if ((docRefId != null) && (!docRefId.isEmpty())) {
					String key = "_id";

					Criteria criteria = Criteria.where(key).in(docRefId);
					Update update = new Update();
					update.set("newsSummary", editedNewsSummary);
					mongoTemplate.updateFirst(new Query(criteria), update, entityClass.getClass().getName().toLowerCase());
				}
		
	}

	

	@Override
	public void updateUnlistedCustomizedSummaryReportDocumentBasedOnId(HistoricalNewsItemDoc newsItem,
			String customizedNewsSummary, String docRefId, BaseDocumentEntity entity) {
		if ((docRefId != null) && (!docRefId.isEmpty())) {
			String key = "_id";

			Criteria criteria = Criteria.where(key).in(docRefId);
			Update update = new Update();
			update.set("customizedNewsSummary", customizedNewsSummary);
			update.set("newsItem", newsItem);
			mongoTemplate.updateFirst(new Query(criteria), update, entity.getClass().getName().toLowerCase());
		}
		
	}

	@Override
	public void updateUnlistedSummaryReportDocumentBasedOnId(String editedNewsSummary, String docRefId,
			BaseDocumentEntity entity) {
		if ((docRefId != null) && (!docRefId.isEmpty())) {
			String key = "_id";

			Criteria criteria = Criteria.where(key).in(docRefId);
			Update update = new Update();
			update.set("newsSummary", editedNewsSummary);
			mongoTemplate.updateFirst(new Query(criteria), update, entity.getClass().getName().toLowerCase());
		}
		
	}
	
	
	@Override
	public <T> List<T> queryDocumentsBasedOnDocRefIds(
			Class<T> entityClass, String collectionName, String documentRefId) {
		if ((documentRefId != null) && (!documentRefId.isEmpty())) {
		String key = "document_ref_id";

		Criteria criteria = Criteria.where(key).in(documentRefId);

		log.info(criteria.toString());
		
		return mongoTemplate.find(new Query(criteria), entityClass, collectionName);
	}
	return null;
	}


	@Override
	public void updateTableCsvInMongo(String tableCsv, String mongoRefId, BaseDocumentEntity entity) {
		// TODO Auto-generated method stub
		if ((mongoRefId != null) && (!mongoRefId.isEmpty())) {
			String key = "_id";

			Criteria criteria = Criteria.where(key).in(mongoRefId);
			Update update = new Update();
			update.set("table_csv", tableCsv);
			mongoTemplate.updateFirst(new Query(criteria), update, entity.getClass().getName().toLowerCase());
			
		}
	}

	@Override
	public void deleteDocTypeTrainingDocumentById(String documentRefId, BaseDocumentEntity entityClass) {
		// TODO Auto-generated method stub
		if ((documentRefId != null) && (!documentRefId.isEmpty())) {
			String key = "document_ref_id";

			Criteria criteria = Criteria.where(key).in(documentRefId);

		    mongoTemplate.remove(new Query(criteria),entityClass.getClass().getName().toLowerCase());
		   // mongoTemplate.remove(object, collection);
		}
	}
	
}
