package com.finstinct.pipeline.crawler.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unlisted_news_source_scheduler_config")
public class UnlistedNewsSourceSchedulerConfig {
	
	
	@Id
	@GeneratedValue
	@Column(name = "SOURCE_ID")
	private Integer sourceId;
	
	@Column(name = "SOURCE_NAME")
	private String sourceName;
	
	@Column(name = "CRON_CONFIG")
	private String cronConfig;
	
	@Column(name = "NO_OF_YRS")
	private int noOfYrs;
	
	@Column(name = "CONFIG_TYPE")
	private String configType;
	
	@Column(name = "CURRENT_DAY_HOUR")
	private int currentDayHour;
	
	@Column(name = "PREVIOUS_DAY_HOUR")
	private int previousDayHour;
	
	@Column(name = "NO_OF_PAGES")
	private int noOfPages;
	
	@Column(name = "SEARCH_URL")
	private String searchUrl;
	
	@Column(name = "SEARCH_LINK_TAGS")
	private String searchLinkTags;
	
	@Column(name = "NEWS_LINK_TAGS")
	private String newsLinkTags;
	
	@Column(name = "HTTPS_PROTOCOL")
	private String httpsProtocol;
	
	@Column(name = "NEWS_PUBLISHED_DATE_TAG")
	private String newsPublishedDateTag;
	
	@Column(name = "NEWS_HEADLINE_TAG")
	private String newsHeadlineTag;
	
	@Column(name = "NEWS_BODY_TAG")
	private String newsBodyTag;
	
	@Column(name = "NEWS_DATE_FORMAT")
	private String newsDateFormat;
	
	@Column(name = "CONVERSION_DATE_FORMAT")
	private String conversionDateFormat;
	
	@Column(name = "GOOGLE_ENHANCED")
	private boolean googleEnhanced;
	
	@Column(name = "PAGE_INITIAL_NO")
	private int pageInitialNo;
	
	@Column(name = "PAGE_OFFSET_NO")
	private int pageOffsetNo;
	
	@Column(name = "NEWS_DATE_TAG_ATTR")
	private String newsDateTagAttr;
	
	
	public String getNewsDateTagAttr() {
		return newsDateTagAttr;
	}

	public void setNewsDateTagAttr(String newsDateTagAttr) {
		this.newsDateTagAttr = newsDateTagAttr;
	}

	public int getPageInitialNo() {
		return pageInitialNo;
	}

	public void setPageInitialNo(int pageInitialNo) {
		this.pageInitialNo = pageInitialNo;
	}

	public int getPageOffsetNo() {
		return pageOffsetNo;
	}

	public void setPageOffsetNo(int pageOffsetNo) {
		this.pageOffsetNo = pageOffsetNo;
	}

	public boolean isGoogleEnhanced() {
		return googleEnhanced;
	}

	public void setGoogleEnhanced(boolean googleEnhanced) {
		this.googleEnhanced = googleEnhanced;
	}

	public String getConversionDateFormat() {
		return conversionDateFormat;
	}

	public void setConversionDateFormat(String conversionDateFormat) {
		this.conversionDateFormat = conversionDateFormat;
	}

	public String getNewsDateFormat() {
		return newsDateFormat;
	}

	public void setNewsDateFormat(String newsDateFormat) {
		this.newsDateFormat = newsDateFormat;
	}

	public String getNewsPublishedDateTag() {
		return newsPublishedDateTag;
	}

	public void setNewsPublishedDateTag(String newsPublishedDateTag) {
		this.newsPublishedDateTag = newsPublishedDateTag;
	}

	public String getNewsHeadlineTag() {
		return newsHeadlineTag;
	}

	public void setNewsHeadlineTag(String newsHeadlineTag) {
		this.newsHeadlineTag = newsHeadlineTag;
	}

	public String getNewsBodyTag() {
		return newsBodyTag;
	}

	public void setNewsBodyTag(String newsBodyTag) {
		this.newsBodyTag = newsBodyTag;
	}

	public String getHttpsProtocol() {
		return httpsProtocol;
	}

	public void setHttpsProtocol(String httpsProtocol) {
		this.httpsProtocol = httpsProtocol;
	}

	public String getSearchLinkTags() {
		return searchLinkTags;
	}

	public void setSearchLinkTags(String searchLinkTags) {
		this.searchLinkTags = searchLinkTags;
	}

	public String getNewsLinkTags() {
		return newsLinkTags;
	}

	public void setNewsLinkTags(String newsLinkTags) {
		this.newsLinkTags = newsLinkTags;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getCronConfig() {
		return cronConfig;
	}

	public void setCronConfig(String cronConfig) {
		this.cronConfig = cronConfig;
	}

	public int getNoOfYrs() {
		return noOfYrs;
	}

	public void setNoOfYrs(int noOfYrs) {
		this.noOfYrs = noOfYrs;
	}
	
	public String getConfigType() {
		return configType;
	}

	public void setConfigType(String configType) {
		this.configType = configType;
	}
	
	public int getCurrentDayHour() {
		return currentDayHour;
	}

	public void setCurrentDayHour(int currentDayHour) {
		this.currentDayHour = currentDayHour;
	}

	public int getPreviousDayHour() {
		return previousDayHour;
	}

	public void setPreviousDayHour(int previousDayHour) {
		this.previousDayHour = previousDayHour;
	}

	public int getNoOfPages() {
		return noOfPages;
	}

	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}

	public String getSearchUrl() {
		return searchUrl;
	}

	public void setSearchUrl(String searchUrl) {
		this.searchUrl = searchUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((configType == null) ? 0 : configType.hashCode());
		result = prime * result + ((conversionDateFormat == null) ? 0 : conversionDateFormat.hashCode());
		result = prime * result + ((cronConfig == null) ? 0 : cronConfig.hashCode());
		result = prime * result + currentDayHour;
		result = prime * result + (googleEnhanced ? 1231 : 1237);
		result = prime * result + ((httpsProtocol == null) ? 0 : httpsProtocol.hashCode());
		result = prime * result + ((newsBodyTag == null) ? 0 : newsBodyTag.hashCode());
		result = prime * result + ((newsDateFormat == null) ? 0 : newsDateFormat.hashCode());
		result = prime * result + ((newsDateTagAttr == null) ? 0 : newsDateTagAttr.hashCode());
		result = prime * result + ((newsHeadlineTag == null) ? 0 : newsHeadlineTag.hashCode());
		result = prime * result + ((newsLinkTags == null) ? 0 : newsLinkTags.hashCode());
		result = prime * result + ((newsPublishedDateTag == null) ? 0 : newsPublishedDateTag.hashCode());
		result = prime * result + noOfPages;
		result = prime * result + noOfYrs;
		result = prime * result + pageInitialNo;
		result = prime * result + pageOffsetNo;
		result = prime * result + previousDayHour;
		result = prime * result + ((searchLinkTags == null) ? 0 : searchLinkTags.hashCode());
		result = prime * result + ((searchUrl == null) ? 0 : searchUrl.hashCode());
		result = prime * result + ((sourceId == null) ? 0 : sourceId.hashCode());
		result = prime * result + ((sourceName == null) ? 0 : sourceName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnlistedNewsSourceSchedulerConfig other = (UnlistedNewsSourceSchedulerConfig) obj;
		if (configType == null) {
			if (other.configType != null)
				return false;
		} else if (!configType.equals(other.configType))
			return false;
		if (conversionDateFormat == null) {
			if (other.conversionDateFormat != null)
				return false;
		} else if (!conversionDateFormat.equals(other.conversionDateFormat))
			return false;
		if (cronConfig == null) {
			if (other.cronConfig != null)
				return false;
		} else if (!cronConfig.equals(other.cronConfig))
			return false;
		if (currentDayHour != other.currentDayHour)
			return false;
		if (googleEnhanced != other.googleEnhanced)
			return false;
		if (httpsProtocol == null) {
			if (other.httpsProtocol != null)
				return false;
		} else if (!httpsProtocol.equals(other.httpsProtocol))
			return false;
		if (newsBodyTag == null) {
			if (other.newsBodyTag != null)
				return false;
		} else if (!newsBodyTag.equals(other.newsBodyTag))
			return false;
		if (newsDateFormat == null) {
			if (other.newsDateFormat != null)
				return false;
		} else if (!newsDateFormat.equals(other.newsDateFormat))
			return false;
		if (newsDateTagAttr == null) {
			if (other.newsDateTagAttr != null)
				return false;
		} else if (!newsDateTagAttr.equals(other.newsDateTagAttr))
			return false;
		if (newsHeadlineTag == null) {
			if (other.newsHeadlineTag != null)
				return false;
		} else if (!newsHeadlineTag.equals(other.newsHeadlineTag))
			return false;
		if (newsLinkTags == null) {
			if (other.newsLinkTags != null)
				return false;
		} else if (!newsLinkTags.equals(other.newsLinkTags))
			return false;
		if (newsPublishedDateTag == null) {
			if (other.newsPublishedDateTag != null)
				return false;
		} else if (!newsPublishedDateTag.equals(other.newsPublishedDateTag))
			return false;
		if (noOfPages != other.noOfPages)
			return false;
		if (noOfYrs != other.noOfYrs)
			return false;
		if (pageInitialNo != other.pageInitialNo)
			return false;
		if (pageOffsetNo != other.pageOffsetNo)
			return false;
		if (previousDayHour != other.previousDayHour)
			return false;
		if (searchLinkTags == null) {
			if (other.searchLinkTags != null)
				return false;
		} else if (!searchLinkTags.equals(other.searchLinkTags))
			return false;
		if (searchUrl == null) {
			if (other.searchUrl != null)
				return false;
		} else if (!searchUrl.equals(other.searchUrl))
			return false;
		if (sourceId == null) {
			if (other.sourceId != null)
				return false;
		} else if (!sourceId.equals(other.sourceId))
			return false;
		if (sourceName == null) {
			if (other.sourceName != null)
				return false;
		} else if (!sourceName.equals(other.sourceName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UnlistedNewsSourceSchedulerConfig [sourceId=" + sourceId + ", sourceName=" + sourceName
				+ ", cronConfig=" + cronConfig + ", noOfYrs=" + noOfYrs + ", configType=" + configType
				+ ", currentDayHour=" + currentDayHour + ", previousDayHour=" + previousDayHour + ", noOfPages="
				+ noOfPages + ", searchUrl=" + searchUrl + ", searchLinkTags=" + searchLinkTags + ", newsLinkTags="
				+ newsLinkTags + ", httpsProtocol=" + httpsProtocol + ", newsPublishedDateTag=" + newsPublishedDateTag
				+ ", newsHeadlineTag=" + newsHeadlineTag + ", newsBodyTag=" + newsBodyTag + ", newsDateFormat="
				+ newsDateFormat + ", conversionDateFormat=" + conversionDateFormat + ", googleEnhanced="
				+ googleEnhanced + ", pageInitialNo=" + pageInitialNo + ", pageOffsetNo=" + pageOffsetNo
				+ ", newsDateTagAttr=" + newsDateTagAttr + "]";
	}

	

	

}
