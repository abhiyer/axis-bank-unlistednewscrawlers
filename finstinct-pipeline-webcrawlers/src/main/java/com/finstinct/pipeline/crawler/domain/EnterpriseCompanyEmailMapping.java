package com.finstinct.pipeline.crawler.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "enterprise_company_email_mapping")
public class EnterpriseCompanyEmailMapping {
	
	
	@Id
	@GeneratedValue
	@Column(name = "ITEM_ID")
	private Integer itemId;
	
	@Column(name = "ENTERPRISE_ID")
	private Integer enterpriseId;
	
	@Column(name = "USER_ID")
	private Integer userId;

    @Column(name = "COMPANY_ID")
	private Integer companyId;
    
    @Column(name = "COMPANY_NAME")
    private String companyName;
    
    @Column(name = "TO_EMAIL")
    private String toEmail;
    
    @Column(name = "CC_EMAIL")
    private String ccEmail;
    
    @Column(name = "DATE_CREATED")
  	private Date dateCreated;

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getCcEmail() {
		return ccEmail;
	}

	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public String toString() {
		return "EnterpriseCompanyEmailMapping [itemId=" + itemId + ", enterpriseId=" + enterpriseId + ", userId="
				+ userId + ", companyId=" + companyId + ", companyName=" + companyName + ", toEmail=" + toEmail
				+ ", ccEmail=" + ccEmail + ", dateCreated=" + dateCreated + "]";
	}
    
    
	
	
}
