package com.finstinct.pipeline.crawler.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unlisted_news_companies")
public class UnlistedNewsCompanies {

	@Id
	@GeneratedValue
	@Column(name = "COMPANY_ID")
	private Integer companyId;
	
	@Column(name = "GROUP_NAME", nullable=false)
	private String groupName;
	
	@Column(name = "COMPANY_NAME", nullable=false)
	private String companyName;
	
	@Column(name = "SYNONYMS", nullable=false)
	private String synonyms;
	
	@Column(name = "PROMOTER_NAMES")
	private String promoterNames;
	
	@Column(name = "CODE")
	private String code;
	
	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "DATE_CREATED")
	private Date dateCreated;

	@Column(name = "ENTERPRISE_ID")
	private Integer enterpriseId;
	
	@Column(name = "USER_NAME")
	private String userName;
	
	@Column(name = "EMAIL_TO")
	private String emailTo;
	
	@Column(name = "EMAIL_CC")
	private String emailCc;
	
	@Column(name = "AUTO_GENERATED_SYNONYMS", nullable = true)
	private String autoGeneratedSynonyms;
	
	
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(String synonyms) {
		this.synonyms = synonyms;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPromoterNames() {
		return promoterNames;
	}

	public void setPromoterNames(String promoterNames) {
		this.promoterNames = promoterNames;
	}

	
	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailCc() {
		return emailCc;
	}

	public void setEmailCc(String emailCc) {
		this.emailCc = emailCc;
	}
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getAutoGeneratedSynonyms() {
		return autoGeneratedSynonyms;
	}

	public void setAutoGeneratedSynonyms(String autoGeneratedSynonyms) {
		this.autoGeneratedSynonyms = autoGeneratedSynonyms;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autoGeneratedSynonyms == null) ? 0 : autoGeneratedSynonyms.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((companyId == null) ? 0 : companyId.hashCode());
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((emailCc == null) ? 0 : emailCc.hashCode());
		result = prime * result + ((emailTo == null) ? 0 : emailTo.hashCode());
		result = prime * result + ((enterpriseId == null) ? 0 : enterpriseId.hashCode());
		result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
		result = prime * result + ((promoterNames == null) ? 0 : promoterNames.hashCode());
		result = prime * result + ((synonyms == null) ? 0 : synonyms.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnlistedNewsCompanies other = (UnlistedNewsCompanies) obj;
		if (autoGeneratedSynonyms == null) {
			if (other.autoGeneratedSynonyms != null)
				return false;
		} else if (!autoGeneratedSynonyms.equals(other.autoGeneratedSynonyms))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (companyId == null) {
			if (other.companyId != null)
				return false;
		} else if (!companyId.equals(other.companyId))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (emailCc == null) {
			if (other.emailCc != null)
				return false;
		} else if (!emailCc.equals(other.emailCc))
			return false;
		if (emailTo == null) {
			if (other.emailTo != null)
				return false;
		} else if (!emailTo.equals(other.emailTo))
			return false;
		if (enterpriseId == null) {
			if (other.enterpriseId != null)
				return false;
		} else if (!enterpriseId.equals(other.enterpriseId))
			return false;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		if (promoterNames == null) {
			if (other.promoterNames != null)
				return false;
		} else if (!promoterNames.equals(other.promoterNames))
			return false;
		if (synonyms == null) {
			if (other.synonyms != null)
				return false;
		} else if (!synonyms.equals(other.synonyms))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UnlistedNewsCompanies [companyId=" + companyId + ", groupName=" + groupName + ", companyName="
				+ companyName + ", synonyms=" + synonyms + ", promoterNames=" + promoterNames + ", code=" + code
				+ ", createdBy=" + createdBy + ", dateCreated=" + dateCreated + ", enterpriseId=" + enterpriseId
				+ ", userName=" + userName + ", emailTo=" + emailTo + ", emailCc=" + emailCc
				+ ", autoGeneratedSynonyms=" + autoGeneratedSynonyms + "]";
	}

		
}
