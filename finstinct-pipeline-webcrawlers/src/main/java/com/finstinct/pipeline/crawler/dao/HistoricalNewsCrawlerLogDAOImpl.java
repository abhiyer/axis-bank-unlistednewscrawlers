package com.finstinct.pipeline.crawler.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finstinct.pipeline.crawler.domain.EnterpriseCompanyEmailMapping;
import com.finstinct.pipeline.crawler.domain.HistoricalNewsCrawlerLog;

@Repository
@Transactional
public class HistoricalNewsCrawlerLogDAOImpl extends BaseDAOImpl<HistoricalNewsCrawlerLog> implements HistoricalNewsCrawlerLogDAO{

	@Override
	public void addUpdateHistoricalNewsCrawlerLog(HistoricalNewsCrawlerLog historicalNewsCrawlerLog) {
		// TODO Auto-generated method stub
		if (historicalNewsCrawlerLog.getHistoricalNewsCrawlerLogId() == null) {
			save(historicalNewsCrawlerLog);
		} else {
			update(historicalNewsCrawlerLog);
		}
	}

	
	@Override
	public List<HistoricalNewsCrawlerLog> getlistOfCrawlersCompletionStatus(Integer companyId) {
		// TODO Auto-generated method stub
		return getEntityManager()
				.createQuery("SELECT en from HistoricalNewsCrawlerLog en WHERE en.companyId = :companyId",
						HistoricalNewsCrawlerLog.class)
				.setParameter("companyId", companyId).getResultList();
	}

}
