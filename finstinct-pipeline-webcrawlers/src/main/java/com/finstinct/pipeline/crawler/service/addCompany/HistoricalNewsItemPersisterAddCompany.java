package com.finstinct.pipeline.crawler.service.addCompany;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finstinct.pipeline.crawler.dao.HistoricalNewsItemDAO;
import com.finstinct.pipeline.crawler.domain.HistoricalNewsItem;
import com.finstinct.pipeline.crawler.historicalnews.HistoricalNewsFeedItems;

@Component
public class HistoricalNewsItemPersisterAddCompany {

	private static final Logger LOG = LoggerFactory.getLogger(HistoricalNewsItemPersisterAddCompany.class);

	@Autowired
	private HistoricalNewsItemDAO historicalNewsItemDao;

	@Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
	public void storeNewsItemToDB(final HistoricalNewsFeedItems newsItems, final Map<Integer, String> stocks,
			final String unlistedNewsItemDocId, String newsDataObject) {
		
		try {
		System.out.println("........ In Store news to DB ..........");
		HistoricalNewsItem historicalNewsItem = new HistoricalNewsItem();
		Date date = new Date();
		Integer[] entityIds = new Integer[1];
		historicalNewsItem.setEntityId(stocks.keySet().toArray(entityIds)[0]);
		// historicalNewsItem.setEnterpriseId(newsItems.getEnterpriseId());
		historicalNewsItem.setPublishDate(newsItems.getPublishedDate());
		historicalNewsItem.setSourceId(newsItems.getSourceId());
		historicalNewsItem.setUrl(newsItems.getUrl());
		historicalNewsItem.setItemDate(date);
		historicalNewsItem.setStatus("new");
		historicalNewsItem.setNameFound(newsItems.getNameFound());

		// if (newsItems.getEnterpriseId() == 7) {
		historicalNewsItem.setIsEmailSent(false);
		historicalNewsItem.setHandoffLog("pending");
		// } else {
		// historicalNewsItem.setIsEmailSent(true);
		// historicalNewsItem.setHandoffLog("NA");
		// }

		// Moved sending Email and SalesForce API call to separate Scheduler
		// 2Dec19
		// if(newsItems.getCode() == null){
		// historicalNewsItem.setHandoffLog("Code is null");
		// }else{
		// historicalNewsItem.setHandoffLog("Completed");
		// }
		// Integer[] total = CalcUtil.calcTotalSentiments(eventsTriggers);
		//
		// historicalNewsItem.setTotalPositive(total[0]);
		// historicalNewsItem.setTotalNeutral(total[1]);
		// historicalNewsItem.setTotalNegative(total[2]);

		historicalNewsItem.setTriggeredDocRef(unlistedNewsItemDocId);
		historicalNewsItem.setItemDataObject(newsDataObject);
		//System.out.println(historicalNewsItem.getItemDataObject());
		historicalNewsItemDao.addUpdateHistoricalNewsItem(historicalNewsItem);
		//System.out.println("DB id for historical news:" + historicalNewsItem.getItemId());
		LOG.debug("DB id for historical news : " + historicalNewsItem.getItemId());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
