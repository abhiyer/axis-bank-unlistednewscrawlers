package com.finstinct.pipeline.crawler.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "historical_customized_summary_report")
public class HistoricalCustomizedSummaryReport {

	@Id
	@GeneratedValue
	@Column(name = "ITEM_ID")
	private Integer itemId;

	@Column(name = "ENTITY_ID")
	private Integer entityId;

	@Column(name = "ENTERPRISE_ID")
	private Integer enterpriseId;

	@Column(name = "ITEM_DATE")
	private Date itemDate;

	@Column(name = "LINK_HASH")
	private String linkHash;

	@Column(name = "SOURCE_ID")
	private Integer sourceId;

	@Column(name = "PUBLICATION_ID")
	private Integer publicationId;

	@Column(name = "CUSTOMIZED_NEWS_DOC_REF")
	private String customizedNewsDocRef;

	@Column(name = "UPDATE_TIME")
	private Date updateTime;

	@Column(name = "URL")
	private String url;

	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "HISTORICAL_NEWS_ITEM_ID")
	private Integer historicalNewsItemId;

	@Column(name = "DUPLICATE_REFRENCE_ID")
	private String duplicateReferenceId;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Column(name = "DOCUMENT_REF_ID", length = 50)
	private String documentRefId;
	
	@Column(name = "IMAGE_URL")
	private String imageUrl;

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public Date getItemDate() {
		return itemDate;
	}

	public void setItemDate(Date itemDate) {
		this.itemDate = itemDate;
	}

	public String getLinkHash() {
		return linkHash;
	}

	public void setLinkHash(String linkHash) {
		this.linkHash = linkHash;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getPublicationId() {
		return publicationId;
	}

	public void setPublicationId(Integer publicationId) {
		this.publicationId = publicationId;
	}

	public String getCustomizedNewsDocRef() {
		return customizedNewsDocRef;
	}

	public void setCustomizedNewsDocRef(String customizedNewsDocRef) {
		this.customizedNewsDocRef = customizedNewsDocRef;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getHistoricalNewsItemId() {
		return historicalNewsItemId;
	}

	public void setHistoricalNewsItemId(Integer historicalNewsItemId) {
		this.historicalNewsItemId = historicalNewsItemId;
	}

	public String getDuplicateReferenceId() {
		return duplicateReferenceId;
	}

	public void setDuplicateReferenceId(String duplicateReferenceId) {
		this.duplicateReferenceId = duplicateReferenceId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDocumentRefId() {
		return documentRefId;
	}

	public void setDocumentRefId(String documentRefId) {
		this.documentRefId = documentRefId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "HistoricalCustomizedSummaryReport [itemId=" + itemId + ", entityId=" + entityId + ", enterpriseId="
				+ enterpriseId + ", itemDate=" + itemDate + ", linkHash=" + linkHash + ", sourceId=" + sourceId
				+ ", publicationId=" + publicationId + ", customizedNewsDocRef=" + customizedNewsDocRef
				+ ", updateTime=" + updateTime + ", url=" + url + ", status=" + status + ", historicalNewsItemId="
				+ historicalNewsItemId + ", duplicateReferenceId=" + duplicateReferenceId + ", fileName=" + fileName
				+ ", documentRefId=" + documentRefId + ", imageUrl=" + imageUrl + "]";
	}

	
	
	
	
}
