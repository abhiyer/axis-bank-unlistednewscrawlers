package com.finstinct.pipeline.crawler.dao;

import java.util.List;

import com.finstinct.pipeline.crawler.domain.HistoricalNewsCrawlerLog;

public interface HistoricalNewsCrawlerLogDAO extends BaseDAO<HistoricalNewsCrawlerLog> {

	void addUpdateHistoricalNewsCrawlerLog(HistoricalNewsCrawlerLog historicalNewsCrawlerLog);

	List<HistoricalNewsCrawlerLog> getlistOfCrawlersCompletionStatus(Integer companyId);

}
